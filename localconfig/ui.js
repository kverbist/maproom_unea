var uicoreConfig = uicoreConfig || {};
/* set google Analytics Id */
/* uicoreConfig.GoogleAnalyticsId='UA-....'; */
uicoreConfig.GoogleAnalyticsId='UA-24207365-2';
/* enable helpmail button */
/* uicoreConfig.helpemail='help@iri.columbia.edu'; */
/* enable GoogleSearch */
/* uicoreConfig.GoogleSearchCX='00....'; */
/* additional default uicoreConfig settings */
uicoreConfig.resolutionQueryServers["default"]= "http://www.climatedatalibrary.cl/";
