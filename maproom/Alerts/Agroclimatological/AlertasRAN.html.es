<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#"
      version="XHTML+RDFa 1.0">
   <head>
      <title>Alertas Agrometeorol&#243;gicas RAN</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en"
            href="AlertasRAN.html?Set-Language=en" />
      <link class="share" rel="canonical" href="AlertasRAN.html" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Agroclimatological_term" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon"
            href="http://www.climatedatalibrary.cl/expert/%28.Precipitation%20.Hourly%20.hourly_precip%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Precipitation/.Hourly/lon/lat/2/copy/hourly_precip/6/-1/roll/pop/T/last/2/24/div/sub/VALUE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig/antialias/true/psdef/fntsze/20/psdef%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/lon/lat/2/copy/hourly_veloc_vientol/6/-1/roll/pop/T/last/2/24/div/sub/VALUE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28.Atmos_Pressure%20.Hourly%20.hourly_atmos_pressure%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Atmos_Pressure/.Hourly/lon/lat/2/copy/hourly_atmos_pressure/6/-1/roll/pop/T/last/1/24/div/sub/VALUE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/lon/lat/2/copy/temp_min/6/-1/roll/pop/T/last/2/24/div/sub/VALUE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/lon/lat/2/copy/temp_max/6/-1/roll/pop/T/last/2/24/div/sub/VALUE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+534+psdef//antialias+true+psdef//fntsze+20+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg dlauximg dlimgts" name="anal" type="hidden" data-default=".Temperature .MinTemp .Hourly .temp_min" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="dlimgts dlimgloc share" name="region" type="hidden" />
         <input class="pickarea admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Chile:RAN:Precipitation:Hourly:ds" />
         
         
         
      </form>
      
      
      <div class="controlBar">
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            <legend>Chile</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Alerts/" shape="rect">Alertas</a> 
            
            
         </fieldset> 
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Agroclimatological_term"><span property="term:label">Alertas Agrometeorológicas</span></legend>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem">
            
            
            <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               
               <option value="">Chile</option>
               
               <optgroup class="template" label="Region">
                  
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                  
               </optgroup></select>
            
            
         </fieldset>
         
         
         <fieldset class="navitem">
            		
            <legend>Variable</legend>
            	<span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value=".Precipitation .Hourly .hourly_precip">Precipitación</option>
               <option value=".Wind_Speed .hourly .hourly_veloc_vientol">Velocidad de Viento</option>
               <option value=".Temperature .MinTemp .Hourly .temp_min">Temperatura Mínima</option>
               <option value=".Temperature .MaxTemp .Hourly .temp_max">Temperatura Máxima</option></select></fieldset>
         
         
         
         
         <fieldset class="navitem">
            
            <legend>Selecciona estaci&#243;n RAN</legend>	
            
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               
               <optgroup class="template" label="Label">
                  
                  <option class="iridl:values region@value label"></option>
                  
               </optgroup></select>
            
            
         </fieldset>
         
         
         
      </div>
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            
            <li><a href="#tabs-1">Descripción</a></li>
            
            <li><a href="#tabs-2">Más información</a></li>
            
            <li><a href="#tabs-3">Fuente</a></li>
            
            <li><a href="#tabs-4">Soporte</a></li>
            
         </ul>
         
         
         
         <fieldset class="regionwithinbbox dlimage" about="">
            
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
               
            </div>
            
            
            <div style="float: left;">
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/(irids%3ASOURCES%3AChile%3ARAN%3APrecipitation%3AHourly%3Anombre%40Buin%3Ads)//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  
                  <div class="template" style="color : black">Estaci&#243;n de la Red Agroclim&#225;tica Nacional: <b><span class="iridl:long_name"></span></b>
                     
                     
                  </div>
                  		
               </div>
               
            
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/.hourly_veloc_vientol/T/last/dup/1/sub/exch/RANGE/%5BT%5Dmaxover/nombre/(irids%3ASOURCES%3AChile%3ARAN%3APrecipitation%3AHourly%3Anombre%40Buin%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Velocidad%20de%20viento%20m%C3%A1ximo%20en%20las%20%C3%BAltimas%2024%20horas%20(m/s)%3A)/def/SOURCES/.Chile/.RAN/.Precipitation/.Hourly/.hourly_precip/T/last/dup/1/sub/exch/RANGE/%5BT%5Dsum/nombre/(irids%3ASOURCES%3AChile%3ARAN%3APrecipitation%3AHourly%3Anombre%40Buin%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Precipitaci%C3%B3n%20total%20en%20las%20%C3%BAltimas%2024%20horas%20(mm)%3A)/def/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/.temp_min/T/last/dup/1/sub/exch/RANGE/%5BT%5Dminover/nombre/(irids%3ASOURCES%3AChile%3ARAN%3APrecipitation%3AHourly%3Anombre%40Buin%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Temperatura%20M%C3%ADnima%20en%20las%20%C3%BAltimas%2024%20horas%20(grados%20C)%3A)/def/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/.temp_max/T/last/dup/1/sub/exch/RANGE/%5BT%5Dmaxover/nombre/(irids%3ASOURCES%3AChile%3ARAN%3APrecipitation%3AHourly%3Anombre%40Buin%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Temperatura%20M%C3%A1xima%20%20en%20las%20%C3%BAltimas%2024%20horas%20(grados%20C)%3A)/def/4/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        
                        <tr style="color : black">
                           
                           <td class="name " rowspan="1" colspan="1"></td>
                           
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                           
                        </tr>
                        
                     </table>
                     
                     
                  </div>
                  
                  
               </div>
               
               
            </div>
            
            
            <div class="dlimgtsbox">
               <img class="dlimgts"
                    src="http://www.climatedatalibrary.cl/expert/expert/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.Chile/.RAN/.Precipitation/.Hourly/.hourly_precip/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm/h%29/def/precip_colors/DATA/AUTO/AUTO/RANGE/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7B/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/.hourly_veloc_vientol/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Velocidad%20de%20Viento%29/def/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/.temp_min/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Temperatura%20M%C3%ADnima%29/def//units/%28grados%20C%29/def/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/.temp_max/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Temperatura%20M%C3%A1xima%29/def//units/%28grados%20C%29/def/dup/T/fig:/colorbars2/:fig%7Dif/nombre/last/plotvalue/+.gif" />
               
               
            </div>
            
            <div class="dlimgtsbox">
               <img class="dlimgts"
                    src="http://www.climatedatalibrary.cl/expert/expert/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.Chile/.RAN/.Precipitation/.Hourly/.hourly_precip/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm/h%29/def/precip_colors/DATA/AUTO/AUTO/RANGE/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7B/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/.hourly_veloc_vientol/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Velocidad%20de%20Viento%29/def/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/.temp_min/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Temperatura%20M%C3%ADnima%29/def//units/%28grados%20C%29/def/dup/T/fig:/colorbars2/:fig%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/.temp_max/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Hourly:nombre%40Buin:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE//long_name/%28Temperatura%20M%C3%A1xima%29/def//units/%28grados%20C%29/def/dup/T/fig:/colorbars2/:fig%7Dif/nombre/last/plotvalue/+.auxfig/+.gif" />
               
               
            </div>
            
            
         </fieldset>
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Precipitation/.Hourly/lon/lat/2/copy/hourly_precip/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig/antialias/true/psdef/%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/lon/lat/2/copy/hourly_veloc_vientol/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Atmos_Pressure%20.Hourly%20.hourly_atmos_pressure%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Atmos_Pressure/.Hourly/lon/lat/2/copy/hourly_atmos_pressure/6/-1/roll/pop/T/last/1/24/div/sub/VALUE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/lon/lat/2/copy/temp_min/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/lon/lat/2/copy/temp_max/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif/Y/-56.0/-16.0/plotrange//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29/psdef/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Precipitation/.Hourly/lon/lat/2/copy/hourly_precip/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig/antialias/true/psdef/%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/lon/lat/2/copy/hourly_veloc_vientol/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Atmos_Pressure%20.Hourly%20.hourly_atmos_pressure%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Atmos_Pressure/.Hourly/lon/lat/2/copy/hourly_atmos_pressure/6/-1/roll/pop/T/last/1/24/div/sub/VALUE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/lon/lat/2/copy/temp_min/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/lon/lat/2/copy/temp_max/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif/Y/-56.0/-16.0/plotrange//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29/psdef/+.gif"
                 border="0"
                 alt="image" />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/expert/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29//anal/parameter/%28.Precipitation%20.Hourly%20.hourly_precip%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Precipitation/.Hourly/lon/lat/2/copy/hourly_precip/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm/h%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig/antialias/true/psdef/%7Dif//anal/get_parameter/%28.Wind_Speed%20.hourly%20.hourly_veloc_vientol%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Wind_Speed/.hourly/lon/lat/2/copy/hourly_veloc_vientol/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE//long_name/%28Velocidad%20de%20Viento%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Atmos_Pressure%20.Hourly%20.hourly_atmos_pressure%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Atmos_Pressure/.Hourly/lon/lat/2/copy/hourly_atmos_pressure/6/-1/roll/pop/T/last/1/24/div/sub/VALUE//long_name/%28Presi%C3%B3n%20Atmosf%C3%A9rica%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MinTemp%20.Hourly%20.temp_min%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Hourly/lon/lat/2/copy/temp_min/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE//long_name/%28Temperatura%20M%C3%ADnima%29/def//units/%28grados%20C%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif//anal/get_parameter/%28.Temperature%20.MaxTemp%20.Hourly%20.temp_max%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/.MaxTemp/.Hourly/lon/lat/2/copy/temp_max/6/-1/roll/pop/T/last/dup/7/sub/exch/0.08333/sub/RANGE//long_name/%28Temperatura%20M%C3%A1xima%29/def//units/%28grados%20C%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef/%7Dif/Y/-56.0/-16.0/plotrange//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//fntsize/20/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29/psdef/+.auxfig/+.gif" />
            
            
         </fieldset>
         
         
  
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h2 align="center" property="term:title">Alertas Agrometeorol&#243;gicas RAN</h2>
            
            <p align="left" property="term:description">En este maproom se presenta informaci&#243;n sobre las condiciones agrometeorol&#243;gicas actuales de la Red Agrometeorol&#243;gica Nacional (RAN).</p>
            
               
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h2 align="center">M&#225;s informaci&#243;n</h2>
            
            <p> </p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Fuente</h2>
            
            <h4><a class="carry" shape="rect">Alertas Agrometeorol&#243;gicas RAN</a></h4>
            
            <dl class="datasetdocumentation">
               <dt>Data</dt>
               <dd>Informacion Agrometeorol&#243;gica en las estaciones RAN</dd>
               <dt>Fuente</dt>
               <dd>La Red Agrometeorol&#243;gica Nacional (RAN), <a href="http://www.agromet.cl/" shape="rect">Agromet</a></dd>
            </dl>
            
         </div>
         
         <div class="ui-tabs-panel-hidden"> 
            
            <h2 align="center">Fuente de datos</h2>
            
            <p> <a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.RAN"
                  shape="rect">Accede a la base de datos utilizado para crear este maproom.</a> 
            </p>
            
         </div>
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h2 align="center">Helpdesk</h2>v
            <p>
               
               <!--
Contact <a href="mailto:help@iri.columbia.edu?subject=RAN Alerts">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.
--> 
            </p>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>