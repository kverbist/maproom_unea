<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" version="XHTML+RDFa 1.0" xml:lang="es">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="description" xml:lang="es" content="En esta sala de mapa se presentan las herramientas disponibles para monitorear la sequía en la región de O'Higgins." />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="Monitor de Sequías O'Higgins" />
      <meta property="og:description" content="En esta sala de mapa se presentan las herramientas disponibles para monitorear la sequía en la región de O'Higgins." />
      <meta property="og:image" content="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad/a-/-a/X/Y/fig-/colors/-fig/Y/-34.6/-34.12/plotrange/X/-72.2/-71.3/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/+.gif" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <title>Monitor de Sequías O'Higgins</title>
      <link rel="stylesheet" type="text/css" href="../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
      <link rel="canonical" href="index.html" />
      <meta property="maproom:Sort_Id" content="a04" />
      <link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#ohiggins" />
      <link rel="term:icon" href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad/a-/-a/X+Y+fig-+colors+-fig+Y/-34.99/-34.0/plotrange//plotborder+72+psdef//plotaxislength+432+psdef/+.jpg" /><script type="text/javascript" src="../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body>
      
      
      <form name="pageform" id="pageform" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup" name="Set-Language" type="hidden" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem"> 
            
            <legend>Chile</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/" shape="rect">Observatorio Agroclimático</a>
            
         </fieldset> 
         
         <fieldset class="navitem"> 
            
            <legend>Chile</legend> 
            <span class="navtext">Monitor de Sequías O'Higgins</span>
            
         </fieldset> 
         
      </div>
      
      <div>
         
         <div id="content" class="searchDescription">
            
            <h2 property="term:title">Monitor de Sequías O'Higgins</h2>
            
            <p align="left" property="term:description">
               En esta sala de mapa se presentan las herramientas disponibles para monitorear la
               sequía en la región de O'Higgins. 
               
            </p>
            
         </div>
         
         <div class="rightcol">
            <div class="ui-tabs">
               <ul class="ui-tabs-nav">
                  <li><a href="#tabs-1">Monitor de Sequías O'Higgins</a></li>
               </ul>
               <div id="tabs-1" class="ui-tabs-panel">
                  <div class="itemGroup">Monitor de Sequías O'Higgins</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Droughts/ICVS.html">Índice Compuesto de Vulnerabilidad a la Sequía</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Droughts/ICVS.html"><img class="itemImage" src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad/a-/-a/X/Y/fig-/colors/-fig/Y/-34.6/-34.12/plotrange/X/-72.2/-71.3/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/+.gif" /></a></div>
                     <div class="itemDescription">
                        Indicador generado en base al proyecto: “Generación de herramientas para estimación
                        de la vulnerabilidad a la sequía en usuarios del programa PRODESAL de INDAP: Aplicación
                        a las comunas de Pichilemu y Marchigüe, Región del Libertador General Bernardo O’Higgins.”
                         
                        El proyecto que tuvo como propósito contribuir al desarrollo de herramientas para
                        la evaluación de la vulnerabilidad a la sequía a escala de usuarios, en el marco de
                        la Gestión Integral de Riesgos Climáticos. Basado en la aplicación de un enfoque denominado
                        “Convergente” propuesto recientemente por Núñez et al (2017).
                        
                        
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
            </div>
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
            
            
         </fieldset>
         
      </div>
      
   </body>
</html>