<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <meta xml:lang="" property="maproom:Entry_Id" content="ohiggins" />
  <title>&Iacute;ndice Compuesto de Vulnerabilidad a la Sequ&iacute;a </title>
  <link rel="stylesheet" type="text/css" href="../../unesco.css" />
  <script type="text/javascript" src="../../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
  <link class="altLanguage" rel="alternate" hreflang="en" href="ICVS.html?Set-Language=en" />
  <link class="share" rel="canonical" href="ICVS.html" />
  <meta xml:lang="" property="maproom:Entry_Id" content="ohiggins" lang="" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ohiggins"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
  <link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad//long_name/%28ICVS%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig/X/-72.5/-71.2/plotrange/Y/-34.99/-34.0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/+.jpg" />
</head>

<body xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
       <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" /> 
	<input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
       <input class="dlimg dlauximg onlyvar share" name="var" type="hidden" />
	<input class="dlimg" name="plotaxislength" type="hidden" />
	<input class="carry share dlimgloc dlimgts" name="region" type="hidden" />
<input class="carry share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
</form>
  <div class="controlBar">
    <fieldset class="navitem" id="toSectionList">
      <legend>Maproom</legend> <a rev="section" class="navlink carryup" href="/maproom/Ohiggins/">O'Higgins</a>
    </fieldset>
    <fieldset class="navitem" id="chooseSection"> 
      <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ohiggins"><span property="term:label">Monitor de Sequ&iacute;as O'Higgins</span></legend>
    </fieldset> 
    <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/Ohiggins.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
    </fieldset>
    
 	     <fieldset class="navitem"><legend>Promedio espacial sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
		<option value="0.0125">Ubicación puntual</option>
		<option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
		<option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
		<option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Localidad</option>
		</select>
    		<link class="admin" rel="iridl:hasJSON" href="https://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-35:-34:-72:-70:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          	<select class="pageformcopy" name="region">
            	<optgroup class="template" label="Label">
            	<option class="iridl:values region@value label"></option>
              </optgroup>
            	</select>
            </fieldset>
	
  </div>

  <div class="ui-tabs">
        <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>
	
<fieldset class="dlimage regionwithinbbox">
<a class="dlimgts" rel="iridl:hasTable" href="https://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MaxExpectedPrecip/%7B5YR/10YR/50YR/100YR%7Dgrouptogrid//long_name/%28Precipitacion%20Maxima%20Esperada%29def//units/%28mm/anio%29def/M/%28Periodo%20de%20Retorno%29renameGRID/%28bb:-72.04375%2C-38.04375%2C-72.03125%2C-38.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/AUTO/RANGE/table:/1/:table/"></a>
<div style="float: left;">
<img class="dlimgloc" src="https://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.0166%2C-30.0166%2C-71.0%2C-30.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
</div>	

<div style="float: left;">

<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgts" rel="iridl:hasJSON" href="https://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
<div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
</div>
</div>



<div class="valid" style="text-align: top;">
<a class="dlimgloc" rel="iridl:hasJSON" href="https://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad/%7B%7D/%7B/(bb%3A-72.04375%2C-38.04375%2C-72.03125%2C-38.03125)//region/parameter/geoobject%5BX/Y%5Dweighted-average/toi4/%7Dforalldatasets2/%7Ba1%7Dds/.a1/info.json"></a> 
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td rowspan="1" colspan="1">ICVS: </td>
                           <td class="name " rowspan="1" colspan="1"> </td>
                           <td align="right" class="value " rowspan="1" colspan="1"> </td>
                        </tr>
                     </table> 
                     
                  </div>

</div>
</div>


<br />
<img class="dlimgts regionwithinbbox" src="https://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MaxExpectedPrecip/%7B5YR/10YR/50YR/100YR%7Dgrouptogrid//long_name/%28Precipitacion%20Maxima%20Esperada%29def//units/%28mm/anio%29def/M/%28Periodo_de_Retorno%29renameGRID/%28bb:-72.04375%2C-38.04375%2C-72.03125%2C-38.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/dup/Periodo_de_Retorno/fig-/colorbars2/-fig/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
</fieldset>

	
<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad//long_name/(ICVS)/def/X/Y/fig%3A/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/%3Afig/X/-72.5/-71.2/plotrange/Y/-34.99/-34.0/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef/#expert" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad//long_name/%28ICVS%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig/X/-72.5/-71.2/plotrange/Y/-34.99/-34.0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/+.jpg?plotaxislength=500"  border="0" alt="image" /><br />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad//long_name/%28ICVS%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig/X/-72.5/-71.2/plotrange/Y/-34.99/-34.0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/+.auxfig/+.gif?plotaxislength=500" />
</fieldset>

<div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >&Iacute;ndice Compuesto de Vulnerabilidad a la Sequ&iacute;a</h2>
<p align="left" property="term:description"><p>Indicador generado en base al proyecto: &ldquo;Generaci&oacute;n de herramientas para estimaci&oacute;n de la vulnerabilidad a la sequ&iacute;a en usuarios del programa PRODESAL de INDAP: Aplicaci&oacute;n a las comunas de Pichilemu y Marchig&uuml;e, Regi&oacute;n del Libertador General Bernardo O&rsquo;Higgins.&rdquo; &nbsp;</p>
<p>El proyecto que tuvo como prop&oacute;sito contribuir al desarrollo de herramientas para la evaluaci&oacute;n de la vulnerabilidad a la sequ&iacute;a a escala de usuarios, en el marco de la Gesti&oacute;n Integral de Riesgos Clim&aacute;ticos. Basado en la aplicaci&oacute;n de un enfoque denominado &ldquo;Convergente&rdquo; propuesto recientemente por N&uacute;&ntilde;ez et al (2017).</p>
<p></p></p>
<p align="left">  <img src="../../icons/Logo_cazalac" alt="Logo CAZALAC"></img></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;Qu&#233; m&#233;todo fue usada para calcular la precipitaci&#243;n m&#225;xima esperada?</h3>
<p>
El m&#233;todo usado toma en consideraci&#243;n la baja intensidad de datos, los datos son agrupados en regiones que son climatol&#243;gicamente homog&#233;neos cual permite de aplicar estad&#237;sticas m&#225;s robustos.
 </p>
<p>Para poder debilitar el efecto de eventos extremos se usa L-momentos en vez de momentos normales. De esta forma eventos extremos no influencian la selecci&#243;n de la distribuci&#243;n. Este m&#233;todo es seleccionado por que es m&#225;s apropiado en regiones con una variabilidad interanual significativa y series de datos de corta duraci&#243;n.</p>
<p>Informaci&#243;n detallada sobre el m&#233;todo usado se puede encontrar <a href="https://www.cazalac.org/documentos/Guia_Metodologia_Atlas_de_Sequia.pdf">aqu&#237;</a>.</p> 
</div>


<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Fuente de Datos</h2>
<p><a href="https://climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/">Atlas de Sequ&#237;a de America Latina y el Caribe</a>, entregado por el Centro <a href="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/">CAZALAC</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>

<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>

