<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Monitor de Sequías O'Higgins</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
<link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#ohiggins" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.ICVS/.Ohiggins/.vulnerabilidad/a-/-a/X+Y+fig-+colors+-fig+Y/-34.99/-34.0/plotrange//plotborder+72+psdef//plotaxislength+432+psdef/+.jpg" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio Agroclim&#225;tico</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                     <span class="navtext">Monitor de Sequías O'Higgins</span>
            </fieldset> 
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Monitor de Sequías O'Higgins</h2>
<p align="left" property="term:description">
En esta sala de mapa se presentan las herramientas disponibles para monitorear la sequ&iacute;a en la región de O'Higgins.
</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Ohiggins/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ohiggins" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
