{
"@context": {
"links": { "@container": "@list" },
"groups": {}
},
"options": [
{"href": "http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php",
 "title": "UNEA Home"}
],
"groups": [
{
"title": "Socios",
"links": [
{"href": "http://www.meteochile.gob.cl",
 "title": "Direccion Meteorologica de Chile"},
{"href": "http://www.inia.cl",
"title": "Direccion General de Aguas"},
{"href": "http://www.fdf.cl/",
 "title": "Fundacion para el Desarrollo Fruticola"}
]
},
{
"title": "Desarrollado con apoyo de",
"links": [
{"href": "http://www.unesco.org/new/en/santiago/natural-sciences/hydrological-systems-and-global-change/",
 "title": "UNESCO"},
{"href": "http://www.rlc.fao.org/es/",
 "title": "FAO"},
{"href": "http://iri.columbia.edu/",
 "title": "International Research Institute"},
{"href": "http://www.cazalac.org/",
"title": "Centro del Agua para Zonas Aridas"}
]
}
]
}