{
"@context": {
"bb": "http://iridl.ldeo.columbia.edu/ontologies/gisuri/geobb/",
"irigaz": "http://iridl.ldeo.columbia.edu/ontologies/irigaz_frwk.owl#",
"term": "http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#",
"irigaz:id": { "@type": "@id" },
"irigaz:hasPart": { "@container": "@list", "@type": "irigaz:GazEntity"},
"term:label": {"@language": "en"}
},
"irigaz:hasPart": [
{ "irigaz:id": "bb:-72.5:-35:-69.5:-33.85:bb",
  "term:label": "O'Higgins"
}
]
}
