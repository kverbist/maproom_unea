<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>ENSO Bulletins</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" /> 
<link rel="canonical" href="Bulletins_ind.html" />
<link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="Chile" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Bulletins_term"/>
<link rel="term:icon" href="Bulletins/enso_sst.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carry carryup" name="Set-Language" type="hidden" />
<input class="carry carryup" name="bbox" type="hidden" />
</form>
<div class="controlBar">

           <fieldset class="navitem" id="toSectionList">
                           <legend>Chile</legend>
                                               <a rev="section" class="navlink carryup" href="index.html">El Niño-Oscilaci&#243;n del Sur</a>
                                                           </fieldset>
                                                                      <fieldset class="navitem" id="chooseSection">
                                                                                                 <legend>Chile</legend>
                                                            </fieldset>

            
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Boletines ENSO</h2>
<p align="left" property="term:description">
Esta secci&#243;n contiene v&#237;nculos y boletines que monitorean y pronostican los eventos de El Niño, La Niña y la Oscilaci&#243;n del Sur. </p> 


</div>

<div class="rightcol">
            <div class="ui-tabs">
               <ul class="ui-tabs-nav"></ul>
               <div class="ui-tabs-panel">
                  	<div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.ciifen.org/" shape="rect">Bolet&#237;n ENSO CIIFEN</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://iri.columbia.edu/our-expertise/climate/forecasts/enso/current/" shape="rect"><img class="itemImage" src="./Bulletins/CIIFEN.jpg" /></a></div>
                     <div class="itemDescription">Bolet&#237;n mensual sobre el estado de El Niño, La Niña y la Oscilaci&#243;n del Sur realizado por el Centro Internacional por la Investigaci&#243;n del Fen&#243;meno del El Niño (CIIFEN). </div>
                     <div class="itemFooter"></div>
		     </div>
			<div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://iri.columbia.edu/our-expertise/climate/forecasts/enso/current/">IRI ENSO Forecast</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://iri.columbia.edu/our-expertise/climate/forecasts/enso/current/"><img class="itemImage" src="./Bulletins/enso_prob.gif" /></a></div>
                     <div class="itemDescription">El Niño, La Niña y la Oscilaci&#243;n del Sur realizado por el International Research Institute for Climate and Society (IRI). </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://iri.columbia.edu/wp-content/uploads/2014/06/ElNinoBulletinJuneFINAL.pdf">Emerging El Ni&#241;o Conditions: Notes for the Global Health Community (IRI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://iri.columbia.edu/wp-content/uploads/2014/06/ElNinoBulletinJuneFINAL.pdf"><img class="itemImage" src="./Bulletins/enso_globalhealth.png" /></a></div>
                     <div class="itemDescription">Este informe entrega informaci&#243;n para apoyar profesionales de salud p&#250;blica en su monitoreo de comunidades vulnerables y para proveer informaci&#243;n a tiempo para intervenciones con el objetvo de reducir los impactos negativos del ENSO sobre la salud.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.bom.gov.au/climate/enso/#current">El Ni&#241;o Wrap-Up (BOM)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.bom.gov.au/climate/enso/#current"><img class="itemImage" src="./Bulletins/bom_logo_clr.gif" /></a></div>
                     <div class="itemDescription">Este informe semestral sobre El Niño, La Niña y la Oscilación del Sur, del Australian Bureau of Meteorology (BOM).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.bom.gov.au/climate/tropnote/tropnote.shtml">Weekly Tropical Climate Note (BOM)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.bom.gov.au/climate/tropnote/tropnote.shtml"><img class="itemImage" src="./Bulletins/bom_logo_clr.gif" /></a></div>
                     <div class="itemDescription">Un informe semanal del estado del clima en los tr&#243;picos del Australian Bureau of Meteorology (BOM).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/enso_advisory/index.shtml">ENSO Diagnostic Discussion (CPC)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/enso_advisory/index.shtml"><img class="itemImage" src="./Bulletins/noaaleft.jpg" /></a></div>
                     <div class="itemDescription">Un an&#225;lisis mensual de las condiciones actuales del ENSO del Climate Prediction Center (CPC).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.pmel.noaa.gov/tao/jsdisplay/">TAO/TRITON Data Display</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.pmel.noaa.gov/tao/jsdisplay/"><img class="itemImage" src="./Bulletins/noaaleft.jpg" /></a></div>
                     <div class="itemDescription">Datos en tiempo real de las mediciones en el Oc&#233;ano Pac&#237;fico, para un monitoreo de El Ni&#241;o y La Ni&#241;a. TAO es un proyecto del Pacific Marine Environmental Laboratory (PMEL) de NOAA.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.wmo.int/pages/prog/wcp/wcasp/enso_update_latest.html">World Meteorological Organization (WMO) El Ni&#241;o/La Ni&#241;a Update</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.wmo.int/pages/prog/wcp/wcasp/enso_update_latest.html"><img class="itemImage" src="./Bulletins/wmo_stationery_logo_small.jpg" /></a></div>
                     <div class="itemDescription">Un informe quincenal como esfuerzo colaborativo entre el International Research Institute for Climate and Society (IRI) y la Organizaci&#243;n Meteorol&#243;gica Mundial (OMM).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
            </div>
         </div>
         



</div>

<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="cant&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
