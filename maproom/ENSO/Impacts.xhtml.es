<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Impactos de ENSO</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" /> 
<link rel="canonical" href="Impacts.html" />
<link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="Chile" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/.0p5deg/.prob/%28http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CRU/.Mask/.mask_prob/dods%29/readdods/.mask_prob/mul/prcp//Tercile/renameGRID/T//season/renameGRID/Y/-56/-16/RANGE//name//probmap/def//long_name/%28Probabilidad%29/def/season/6.5/VALUE/Tercile//wet/VALUE/ENSO//nino/VALUE/X/Y/fig-/colors/blue/lakes/black/thin/states_gaz/black/thinnish/coasts_gaz/black/thin/countries_gaz/-fig//plotaxislength/550/psdef/X/-85.0/-65.0/plotrange/Y/-56.0/-16/plotrange//XOVY/null/psdef//antialias/true/psdef//fntsize/20/psdef+//antialias+true+psdef//fntsize+20+psdef//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">

           <fieldset class="navitem" id="toSectionList">
                           <legend>Chile</legend>
                                               <a rev="section" class="navlink carryup" href="index.html">ENSO</a>
                                                           </fieldset>
                                                                      <fieldset class="navitem" id="chooseSection">
                                                                                                 <legend>Chile</legend>
                                                                                                                                                     </fieldset>

            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Impactos de ENSO</h2>
<p align="left" property="term:description">Esta secci&#243;n contiene las herramientas que ayudan a explorar la relaci&#243;n hist&#243;rica entre ENSO y el clima en Chile. </p>
<p>Eventos ENSO han sido conectados con casi todos los aspectos de la vida humana: enfermedades, rendimentos altos y bajos en agricultura, desastres naturales, disponilidad de recursos h&#237;dricos, demanda de energ&#237;a, disrupci&#243;n en la generaci&#243;n de hidroel&#233;ctrica, fluctuaciones de precios, fluctuaciones en la captura de peces, movimiento de animales, incendios forestales, la producci&#243;n econ&#243;mica y mucho m&#225;s. </p>
</div>
<div class="rightcol tabbedentries" about="/maproom/ENSO/Impacts.html" >
<ul>
<li class="inactive">
  <link rel="maproom:tabterm" 
href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Climate_Impacts_term"/>
</li>
</ul>
 
</div>



</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="cont&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
