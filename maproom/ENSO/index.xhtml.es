<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>ENSO Cuarto de Mapas</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link rel="canonical" href="index.html" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="Chile" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<meta property="maproom:Sort_Id" content="a03" />

<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy"
href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#enso" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/dynamic_images/ENSO/images/IRI/figure1.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carry carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/">Observatorio Agroclim&#225;tico</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                     <span class="navtext">ENSO</span>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">El Niño, La Niña y la Oscilaci&#243;n del Sur (ENSO)</h2>
<p>
El "ENSO" se refiere a El Niño-Oscilaci&#243;n del Sur, un ciclo de eventos de calentamiento y enfriamiento del Océano Pac&#237;fico ecuatorial y su atm&#243;sfera que ocurre con una frecuencia aproximada de 2 a 7 a&#241;os. Esta variabilidad anual o multi-anual en condiciones oceanogr&#225;ficas y atmosf&#233;ricas tienen impactos de gran alcance, llamado "teleconexiones", sobre la precipitaci&#243;n estacional y los patrones de temperatura en muchos lugares del mundo y particularmente en Chile. 
</p> 
<p align="left" property="term:description"> Este conjunto de datos incluye mapas y análisis útiles para el monitoreo del ENSO, para entender su impacto y para acceder boletines preparados por diferentes organizaciones activas en el monitoreo y el pron&#243;stico del ENSO. 

</p>
</div>
 
  <div class="rightcol tabbedentries" about="/maproom/ENSO/index.html" >
  

<!--<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Diagnostics_term" />
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Drought_term"/>-->


</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="cont&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
