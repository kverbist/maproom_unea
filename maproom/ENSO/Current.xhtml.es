<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Condiciones Actuales El Ni&#241;o</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" /> 
<link rel="canonical" href="Current.html" />
<link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="UNEA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/dynamic_images/ENSO/images/IRI/figure1.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" /> 
</form>
<div class="controlBar">

           <fieldset class="navitem" id="toSectionList">
                           <legend>Chile</legend>
                                               <a rev="section" class="navlink carryup" href="index.html">ENSO</a>
                                                           </fieldset>
                                                                      <fieldset class="navitem" id="chooseSection">
                                                                                                 <legend>ENSO</legend>
                                                                                                                                                     </fieldset>


</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Condiciones Actuales El Ni&#241;o</h2>
<p align="left" property="term:description">
Esta secci&#243;n muestra las condiciones actuales y las proyecciones de El Ni&#241;o.
</p>

<p>
En esta secci&#243;n se muestra informaci&#243;n y gr&#225;ficos actuales del International Research Institute for Climate and Society (IRI) y del NOAA Climate Prediction Center (CPC).
</p>


</div>
<div class="rightcol tabbedentries" about="/maproom/ENSO/Current.html" >
<ul>
<li class="inactive">
  <link rel="maproom:tabterm" 
href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Current_term"/>
</li>
</ul>
 
</div>



</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
