<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Monitoreo de ENSO</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link rel="canonical" href="Diagnostics.html" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Diagnostics.html?Set-Language=en" /> 
<link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="Chile" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy"
href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#diagnostics" />
<link rel="term:icon" href="New/elnino-lalnina-conditions_maproom.jpg" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Chile</legend> 
                    <a rev="section" class="navlink carryup" href="index.html">ENSO</a>
            </fieldset> 
           <fieldset class="navitem" id="chooseSection">
                           <legend>Chile</legend>
             </fieldset> 
            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Monitoreo de ENSO</h2>
<p align="left" property="term:description">Esta secci&#243;n contiene mapas, series de tiempo y otros análisis &#250;tiles para el monitoreo de ENSO y para identificar la presencia de un cambio hacia El Niño o La Niña. </p>
<p>El monitoreo de El Niño y La Niña requiere observaciones de la atm&#243;sfera y del oc&#233;ano. Estas observaciones frecuentemente se representan a trav&#233;s de varios indicadores atmosf&#233;ricos y oceanográficos.</p>
<p>Hay varias regiones en el Océano Pacífico tropical que han sido identificadas para el monitoreo y la identificación del desarrollo de El Niño o La Niña. Referenciado como regiones ni&#241;o, las regiones m&#225;s comunes est&#225;n identificadas en la figura:             
          </p>
          <img class="dsimg" src="New/NINO_Regions.jpg" width="348" />
                               
      <ul>
            <li><i>NINO1+2</i> (0-10S, 80-90W). La regi&#241;n que tipicamente se calienta primero cuando se desarrolla un evento El Niño.</li>
            <li><i>NINO3</i> (5S-5N; 150W-90W). La regi&#241;n que muestra la mayor variabilidad en temperaturas del mar durante eventos ENSO.</li>
            <li><i>NINO3.4</i> (5S-5N; 170W-120W). La regi&#241;n que muestra una gran variabilidad durante eventos ENSO, y que est&#225; m&#225;s cerca (que Ni&#241;o) y que tiene mayor influencia sobre el cambio en el patr&#243;n espacial de la precipitaci&#243;n.</li>
            <li><i>NINO4</i> (5S-5N: 160E-150W). La regi&#241;n donde cambios en la temperatura del mar llegan a valores que superan los 27.5C, que es un umbral importante para la producci&#243;n de precipitaci&#243;n</li>
                               
      </ul>
</div>

<div class="rightcol tabbedentries" about="/maproom/ENSO/Diagnostics.html" >
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Long_x_Time_term"/>
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_SST_Plots_term"/>
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Surface_Wind_term"/>
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Thermocline_term"/>
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Time_Series_term"/>
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Tropical_Atm_Circulation_term"/>

<div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Long_x_Time_term">
<p property="term:gloss">Presenting climatic data with time on the y-axis and longitude on the x-axis highlights the persistence of an analyzed variable over a certain longitude over time or the movement, or wave like propagation, of a variable across longitudes. Also known as Hovmoller plots, the products in this maproom can be used to monitor known patterns that precede ENSO events, including El Niño and La Niña.</p>
</div>
  <div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Thermocline_term">
<p property="term:gloss">The equatorial Pacific Ocean is characterized by a two-layer system consisting of a warm upper layer and a cold, deep layer. The dividing line between these two layers is the thermocline. The position of the thermocline is described by depth, vertical slope, thickness and to a lesser extent, horizontal slope. Changes in the position of the thermocline can be indicators that there are larger scale shifts in the coupled ocean-atmosphere system occurring. These shifts may be precursors to an ENSO event, thus, there may be value in monitoring shifts in the thermocline when evaluating the probability of an El Niño or La Niña.</p>
</div>
  <div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_SST_Plots_term">
<p property="term:gloss">Sea Surface Temperature (SST) is an important element of the coupled interaction of the atmosphere and ocean system in the equatorial Pacific ocean region.  ENSO events during previous years have yielded an understanding of the SST patterns that precede and persist during a particular ENSO event.

These plots afford the ability to track the development and propagation of SST patterns that may precede an ENSO event, monitor the persistence of SST shifts that and compare that information to historical data.  Especially important are the plots showing anomalies, or departures from average, as they highlight the slight shifts in SSTs that can be useful in identifying an ENSO event.</p>
</div>
  <div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Surface_Wind_term">
<p property="term:gloss">Over the equatorial Pacific Ocean, the average (during ENSO neutral) flow of surface winds is westward. During El Niño conditions, the usually westward movement of surface air is replaced by an anomalous eastward flow. This flow leads to subsequent shifts in the thermocline and eventual above average heating of SSTs in the eastern equatorial Pacific Ocean. As the shift in surface wind from a westward to an eastward flow precedes a shift towards an ENSO event, surface wind can be monitored when evaluating the probability of an El Niño or La Niña.

These plots allow for the user to evaluate the current surface winds averaged over different regions of the equatorial Pacific by comparing them to historical values. </p>
</div> 
<div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Time_Series_term">
<p property="term:gloss">Presenting climatic data with time on the x-axis and the analyzed variable on the y-axis highlights the fluctuation of that variable at a certain point or averaged over a desired region, across time. Some of these plots afford the ability to monitor multiple variables and visualize their relationship to each other. Time series plots are valuable tools in identifying known patterns that precede ENSO events, including El Niño and La Niña, as well as monitoring the duration and intensity of such events.  </p>
</div>
<div about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Tropical_Atm_Circulation_term">
<p property="term:gloss">Tropical Atmospheric Circulation refers to the large-scale movement of air and the associated distribution of thermal energy over Earth’s surface. There is a relationship between lower atmosphere (925 mb) and upper atmosphere (200 mb) circulation with Sea Surface Temperatures (SSTs) in the equatorial Pacific Ocean. Velocity potential is a variable useful in monitoring tropical atmospheric circulation, as it is proportional to divergence and convection. 

Over the eastern Equatorial Pacific (NINO3.4) during El Niño, at the 200 mb level, velocity potential is directly proportional to divergence and enhanced convection and as a result, rainfall. At the 925 mb level, the relationship is inverse as negative anomalies of velocity potential indicate areas of enhanced convergence near the ocean surface, enhanced rising motion and convection and as a result, above average rainfall (Japan Meteorological Agency 2014). 

It is important to note that the response of SSTs, convection and rainfall to shifts in the tropical atmospheric circulation during El Niño events (as indicated by velocity potential) is not symmetric to the response of those variables during La Nina.
</p>
</div>

</div>



</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="cont&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
