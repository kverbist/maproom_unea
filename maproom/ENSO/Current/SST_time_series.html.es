<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
      >
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="maproom:Sort_Id" content="c01" />
      <title>Serie de Tiempo Temperatura del Mar</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="SST_time_series.html?Set-Language=en" />
      <link rel="canonical" href="SST_time_series.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_term"/>
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Current_term"/>
      <link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://www.climatedatalibrary.cl/dynamic_images/ENSO/images/cpcncepnoaa/figura2.gif" />
      <script type="text/javascript" src="../../../uicore/uicore.js"></script>
      <script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
 <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryLanguage carryup carry share">
         <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>ENSO</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/ENSO/Current.html">Condiciones Actuales El Ni&#241;o</a>
            
         </fieldset> 
         
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Current_term"><span property="term:label">Serie de Tiempo Temperatura del Mar</span></legend>
            </fieldset> 
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            
            <div id="ifrcdiv">
               
            </div>
            
            <li><a href="#tabs-1">Descripci&#243;n</a></li>
            
            <li><a href="#tabs-2">Instrucciones</a></li>
            
            <li><a href="#tabs-3">Documentaci&#243;n de Datos</a></li>
            
            <li><a href="#tabs-4">Cont&#225;ctanos</a></li>
            
         </ul>
         
         <fieldset class="dlimage" id="content" about="">
         <img class="dlimg" src="http://www.climatedatalibrary.cl/dynamic_images/ENSO/images/cpcncepnoaa/figura2.gif" border="0" alt="image" />
            
         </fieldset>
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h2 align="center" property="term:title">Cual es la evoluci&#243;n actual de la Temperatura del Mar?</h2>
            
            <p align="left" property="term:description">Este gr&#225;fico muestra la evoluci&#243;n de la anomal&#237;a de la temperatura del mar en los &#250;ltimos 12 meses para diferentes &#225;reas del Oc&#233;ano Pac&#237;fico.
            </p>

	     <p align="left">El gr&#225;fico muestra la serie de tiempo de la anomal&#237;a de la temperatura del mar (SST) en las regiones Ni&#241;o 1, 2, 3 and 3.4 con respecto al periodo 1981-2010. </p>

            
            <p align="left">El informe completo est&#225; disponible en la p&#225;gina web del Climate Predction Center (CPC) en NOAA  
               Hacer clic <a href="http://www.cpc.ncep.noaa.gov/products/precip/CWlink/MJO/enso.shtml">aqu&#237;</a>.
            </p>
            
           
         </div>
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h2 align="center">Como navegar entre las im&#225;genes?</h2>
            
            <p><i>Cambiar im&#225;genes:</i>
               Selecciona el imagen de inter&#233;s en el menu arriba, por la derecha del titulo -Condiciones Actuales El Ni&#241;o-.
            </p>
            
            <p><i>Volver al menu inicial del Observatorio:</i>
               Hacer clic en el v&#237;nculo azul llamado -Condiciones Actuales El Ni&#241;o-.
            </p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Documentaci&#243;n de Datos</h2>
            <p align="left">M&#225;s informaci&#225;n se encuentra en la p&#225;gina web del Climate Predction Center (CPC) en NOAA  
               Hacer clic <a href="http://www.cpc.ncep.noaa.gov/products/precip/CWlink/MJO/enso.shtml">aqu&#237;</a>.</p>
            
            
         </div>
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h2 align="center">Helpdesk</h2>
                      
            <p>
               Contactar <a href="mailto:mwar-lac@unesco.org?subject=ENSO current conditions">mwar-lac@unesco.org</a> con preguntas t&#233;cnicas sobre este Map Room.
               
            </p>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
         <fieldset class="navitem langgroup" id="contactus"></fieldset>
         
      </div>
      
   </body>
</html>