<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Probabilidades de Condiciones H&#250;medos o Secos seg&#250;n la Fase de ENSO</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="share" rel="canonical" href="ENSO_PRCP_Prob_TS2p1.html" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="ENSO_PRCP_Prob_TS2p1.html?Set-Language=en" />
      <link class="carryLanguage" rel="home" href="http://www.climatedatalibrary.cl/UNEA/maproom/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
      <link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
      <meta xml:lang="" property="maproom:Entry_Id" content="ENSO_Climate_Impacts_ENSO_PRCP_Prob_TS2p1" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#LandSurface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#PlanetarySurface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Surface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#enso" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Climate_Impacts_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#percentile" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
      <link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/.0p5deg/.prob/%28http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CRU/.Mask/.mask_prob/dods%29/readdods/.mask_prob/mul/prcp//Tercile/renameGRID/T//season/renameGRID/Y/-56/-16/RANGE//name//probmap/def//long_name/%28Probabilidad%29/def/season/6.5/VALUE/Tercile//wet/VALUE/ENSO//nino/VALUE/X/Y/fig-/colors/blue/lakes/black/thin/states_gaz/black/thinnish/coasts_gaz/black/thin/countries_gaz/-fig//plotaxislength/550/psdef/X/-85.0/-65.0/plotrange/Y/-56.0/-16/plotrange//XOVY/null/psdef//antialias/true/psdef//fntsize/20/psdef+//antialias+true+psdef//fntsize+20+psdef//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef+.gif" />
      <script type="text/javascript" src="../../../uicore/uicore.js">
      </script><script type="text/javascript" src="../../../maproom/unesco.js">
      </script>
      </head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
          <input class="carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc share" name="bbox" type="hidden" />
         <input class="carry dlimg dlimgts share" name="season" type="hidden"
                data-default="Jun-Aug" />
         <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" /> 
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="carry share dlimgloc dlimgts admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
         <input class="carry dlimg share" name="Tercile" type="hidden" data-default="wet" />
         <input class="carry dlimg share" name="ENSO" type="hidden" data-default="nino" />
         <input class="dlimg dlauximg" name="plotaxislength" type="hidden" />
      
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>ENSO</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/ENSO/Impacts.html">Impactos del ENSO</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Climate_Impacts_term"><span property="term:label">Per&#250;</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Regi&#243;n</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Region">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
           <fieldset class="navitem"><legend>Temporada</legend><span class="selectvalue"></span><select class="pageformcopy" name="season"><option value="Jan-Mar">Ene-Mar</option><option value="Feb-Apr">Feb-Abr</option><option value="Mar-May">Mar-May</option><option value="Apr-Jun">Abr-Jun</option><option value="May-Jul">May-Jul</option><option value="Jun-Aug">Jun-Ago</option><option value="Jul-Sep">Jul-Sep</option><option value="Aug-Oct">Ago-Oct</option><option value="Sep-Nov">Sep-Nov</option><option value="Oct-Dec">Oct-Dec</option><option value="Nov-Jan">Nov-Ene</option><option value="Dec-Feb">Dec-Feb</option></select></fieldset>
  
          <fieldset class="navitem"><legend>Condici&#243;n</legend><span class="selectvalue"></span><select class="pageformcopy" name="Tercile"><option value="dry">Seco</option><option value="normal">Normal</option><option value="wet">H&#250;medo</option></select></fieldset>
  
          <fieldset class="navitem"><legend>Fase ENSO</legend><span class="selectvalue"></span><select class="pageformcopy" name="ENSO"><option value="nino">El Ni&#241;o</option><option value="nina">La Ni&#241;a</option></select></fieldset>         


 	     <fieldset class="navitem"><legend>Promedio espacial sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
		<option value="0.0125">Ubicación puntual</option>
		<option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
		<option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
		<option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Localidad</option>
		</select>
    		<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          	<select class="pageformcopy" name="region">
            	<optgroup class="template" label="Label">
            	<option class="iridl:values region@value label"></option>
              </optgroup>
            	</select>
            </fieldset>    
         
      </div>
      
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            
            
            <li><a href="#tabs-1">Descripción</a></li>
            
            
            <li><a href="#tabs-2">Documentación</a></li>
            
            
            <li><a href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/">Fuente</a></li>
            
            
            <li><a href="#tabs-3">Contáctanos</a></li> 
            
            
         </ul>
         
         
        <fieldset class="regionwithinbbox dlimage bis" about="">
            <img class="dlimgloc"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.0166%2C-30.0166%2C-71.0%2C-30.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
            
            
            <div class="valid" style="display: inline-block; text-align: top;">
               <a class="dlimgloc" rel="iridl:hasJSON"
                  href="/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
               
               <div class="template" align="center">Observaciones para <span class="bold iridl:long_name"></span></div>
               
            </div>
            <br />
            <br />
            <br />
            
            
            <img class="dlimgts bis1" rel="iridl:hasFigureImage"
                 src="http://www.climatedatalibrary.cl/SOURCES/.UEA/.CRU/.TS2p1/.monthly/.prcp//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm%29/def/%28bb:-71.0%2C-30.0%2C-70.0%2C-29.0%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jun-Aug%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Precipitaci%C3%B3n%20Normal%20-%20Percentil%29/def/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/a:/:a:/T/%28Jan%201950%29/%28Dec%202002%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T/%28Jan%201950%29/%28Dec%202002%29/RANGE/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28Condici%C3%B3n%20ENSO%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/%28Jan%201950%29/last/RANGE/T/fig-/colorbars2/medium/solid/black/line/dashed/black/line/-fig//antialias/true/def/+.gif" />
            <br />
            
            <img class="dlimgts bis2" rel="iridl:hasFigureImage"
                 src="http://www.climatedatalibrary.cl/SOURCES/.UEA/.CRU/.TS2p1/.monthly/.prcp//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm%29/def/%28bb:-71.0%2C-30.0%2C-70.0%2C-29.0%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jun-Aug%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Precipitaci%C3%B3n%20Normal%20-%20Percentil%29/def/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/a:/:a:/T/%28Jan%201950%29/%28Dec%202002%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T/%28Jan%201950%29/%28Dec%202002%29/RANGE/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28Condici%C3%B3n%20ENSO%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/%28Jan%201950%29/last/RANGE/T/fig-/colorbars2/medium/solid/black/line/dashed/black/line/-fig//antialias/true/def/+.auxfig/+.gif" />
            
         </fieldset>
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            <link rel="iridl:hasFigure"
                  href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/.0p5deg/.prob/%28http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CRU/.Mask/.mask_prob/dods%29/readdods/.mask_prob/mul/prcp//Tercile/renameGRID/T//season/renameGRID/Y/-56/-16/RANGE//name//probmap/def//long_name/%28Probabilidad%29/def/X/Y/fig-/colors/blue/lakes/black/thin/states_gaz/black/thinnish/coasts_gaz/black/thin/countries_gaz/-fig//plotaxislength/550/psdef/X/-85.0/-65.0/plotrange/Y/-56.0/-16/plotrange//season/6.5/plotvalue//Tercile//wet/plotvalue//ENSO//nino/plotvalue//XOVY/null/psdef//antialias/true/psdef//fntsize/20/psdef/" />
            <img class="dlimg"
                 src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/.0p5deg/.prob/%28http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CRU/.Mask/.mask_prob/dods%29/readdods/.mask_prob/mul/prcp//Tercile/renameGRID/T//season/renameGRID/Y/-56/-16/RANGE//name//probmap/def//long_name/%28Probabilidad%29/def/X/Y/fig-/colors/blue/lakes/black/thin/states_gaz/black/thinnish/coasts_gaz/black/thin/countries_gaz/-fig//plotaxislength/550/psdef/X/-85.0/-65.0/plotrange/Y/-56.0/-16/plotrange//season/6.5/plotvalue//Tercile//wet/plotvalue//ENSO//nino/plotvalue//XOVY/null/psdef//antialias/true/psdef//fntsize/20/psdef/+.gif"
                 border="0"
                 alt="image" />
            <br />
            <img class="dlauximg"
                 src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/.0p5deg/.prob/%28http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CRU/.Mask/.mask_prob/dods%29/readdods/.mask_prob/mul/prcp//Tercile/renameGRID/T//season/renameGRID/Y/-56/-16/RANGE//name//probmap/def//long_name/%28Probabilidad%29/def/X/Y/fig-/colors/blue/lakes/black/thin/states_gaz/black/thinnish/coasts_gaz/black/thin/countries_gaz/-fig//plotaxislength/550/psdef/X/-85.0/-65.0/plotrange/Y/-56.0/-16/plotrange//season/6.5/plotvalue//Tercile//wet/plotvalue//ENSO//nino/plotvalue//XOVY/null/psdef//antialias/true/psdef//fntsize/20/psdef/+.auxfig/+.gif" />
            
            
         </fieldset>
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h2 align="center" property="term:title">Probabilidades de Condiciones Húmedos o Secos según la Fase de ENSO</h2>
            
            
            <p align="left" property="term:description"> Este mapa muestra la probablidad de encontrar condiciones húmedas o secas según la
               Fase de ENSO (El Niño, La Niña o Normal).
               
            </p>
            
            <p align="left">Se puede utilizar los controles en la parte superior de la página para seleccionar el periodo
               de tres meses de interés, la fase del ENSO y la condición (húmeda, normal o seca). Tambi&#233;n permite visualizar la precipitaci&#243;n anual de un punto de inter&#233;s, con indicaci&#243;n de los a&#241;os 'El Ni&#241;o', 'La Ni&#241;a' y los a&#241;os normales.
            </p>
            
            <p align="left">Para este análisis, se definen los eventos El Niño y La Niña según la anomalía de
               la temperatura superficial del mar en la zona NINO3.4. Se evalúa la relación entre
               la precipitación y la fase del ENSO para periodos consecutivos de tres meses, y el
               mapa muestra la proporción de los años que muestran condiciones húmedas, normales
               o secas.
               		 Como ejemplo, un punto que muestra un valor 0.6 en la categoría 'húmeda' (mayor
               precipitación que lo normal) durante la fase 'Niño', indica que para los años entre
               1950-2002 donde ocurrió un año 'El Niño', 6 de los 10 años mostraron condiciones húmedas
               durante este fenómeno.     
               
            </p>
            
            
                       
            <h4>Referencias</h4>
                      
            <p>
               Mason, S.J. and L. Goddard, 2001: Probabilistic precipitation anomalies associated
               with ENSO. <i>Bull. Amer. Meteor. Soc.</i>, <b>82</b>, 619-638. doi: <a href="http://dx.doi.org/10.1175/1520-0477(2001)082&lt;0619:PPAAWE?2.3.CO;2">http://dx.doi.org/10.1175/1520-0477(2001)082&lt;0619:PPAAWE?2.3.CO;2</a>
               
               
            </p>
            
            
         </div>
         
         
             <div id="tabs-2" class="ui-tabs-panel">
            
            <h2 align="center">Documentaci&#243;n</h2>
            
            <h4>Datos de Precipitaci&#243;n</h4>
            
            <dl class="datasetdocumentation">
               
               <dt>Datos</dt>
               <dd>1950-2002 grilla de datos mensuales (con resoluci&#243; lat/lon de 0.5&#176;) de precipitaci&#243;n total para la &#225;reas de tierra.
               </dd> 
               
               <dt>Fuente de Datos</dt>
               <dd>University of East Anglia (UEA) Climatic Research Unit (CRU) TS2.1 dataset (<a href="http://iridl.ldeo.columbia.edu/SOURCES/.UEA/.CRU/.TS2p1/.monthly/">Data Library entry</a>, <a href="http://www.cru.uea.ac.uk/~timm/grid/CRU_TS_2_1.html">Informaci&#243;n en CRU</a> )
               </dd> 
               
               <dt>Reference</dt>
               <dd>Mitchell, T. D., P. D. Jones, 2005: <a href="http://onlinelibrary.wiley.com/doi/10.1002/joc.1181/abstract">An improved method of constructing a database of monthly climate observations and
                     associated high-resolution grids</a>. <i>Int. J. Climatol.</i> <b>25</b>, 693-712. DOI: 10.1002/joc.1181
               </dd>
               
            </dl>
            
            <h4>Datos de Temperatura Superficial del Mar</h4>
            
            <dl class="datasetdocumentation">
               
               <dt>Data</dt>
               <dd>NINO3.4 &#237;ndice mensual de temperatura del mar superficial para 1950-2002 (promedio espacial sobre la regi&#243;n 5&#176;S
                  a 5&#176;N y 170&#176;W a 120&#176;W).
               </dd>
               
               <dt>Data Source</dt>
               <dd>Acceder a la base de datos en (<a href="http://iridl.ldeo.columbia.edu/SOURCES/.KAPLAN/.EXTENDED/">la librer&#237;a de datos clim&#225;ticos</a>) 
               </dd>
               
               <dt>Referencia</dt>
               <dd>Kaplan, A., M. Cane, Y. Kushnir, A. Clement, M. Blumenthal, and B. Rajagopalan, 1998:
                  <a href="http://onlinelibrary.wiley.com/doi/10.1029/97JC01736/abstract">Analyses of global sea surface temperature 1856-1991</a>. <i>Journal of Geophysical Research</i>. <b>103</b>, 18,567-18,589.  DOI: 10.1029/97JC01736
               </dd>
               
            </dl>
            
         </div>         
         
         <div class="ui-tabs-panel-hidden">
            
            
            <h2 align="center">Fuente</h2>
            
            
            <p>
               <a href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/">Access the dataset used to create this map.</a>
               
               
            </p>
            
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Helpdesk</h2>
            
            <p>
               Contact <a href="mailto:help@iri.columbia.edu?subject=Historical Probability of Seasonal Gridded Precipitation Tercile Conditioned on ENSO">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.
               
            </p>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
         <fieldset class="navitem langgroup" id="contactus"></fieldset>
         
      </div>
      
   </body>
 </html>
