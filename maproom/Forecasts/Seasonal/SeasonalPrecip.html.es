<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Precipitation Forecast from the Chilean Meteo Service (DMC)</title>
<link class="altLanguage" rel="alternate" hreflang="en" href="SeasonalPrecip.html?Set-Language=en" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../unesco.js"></script>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link rel="canonical" href="SeasonalPrecip.html" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Sort_Id" content="" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/lon/lat/2/copy/PearsonR/6/-1/roll/.target_date/S/last/cvsunits//S/parameter/VALUE/dup/1/sub//name//target_start/def/exch/1/add//name//target_end/def/X/Y/fig-/colors/||/black/scatter/scattercolor/plotlabel/plotlabel/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/S/last/cvsunits//S/parameter/7/3/getinterval/interp/nip/plotvalue//plotborderbottom/72/psdef//framelabel/%28%29psdef//PearsonR/-1.0/1.0/plotrange/Y/-56.04167/-16/plotrange/++//T/0.5/plotvalue/Y/-56.0/-16.0/plotrange+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
<style>
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
@media only all and (max-width: 800px) {
.dlimage.ver4 {
        width: 99%;
}
}
.fieldRainfall #PoEV {
display: none;
}
.fieldAnomaly #PoEV {
display: none;
}
.fieldPoM #PoEV {
display: none;
}
.field #PoEV {
display: none;
}
.varPercentile #physThresh {
display: none;
}
.varRainfall #percThresh {
display: none;
}
.var #percThresh {
display: none;
}
.dlcontrol.L {display: none !important;}
</style>
</head>
   <body xml:lang="es">
      
      
      <form name="pageform" id="pageform" method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carryLanguage" name="Set-Language" type="hidden" />
         <input class="dlimg dlauximg dlimg2 share dlimgloc dlimglocclick" name="bbox"
                type="hidden" />
         <input class="share dlimgts dlimgloc dlimglocclick station" name="region"
                type="hidden" />
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="unused" name="plotaxislength" type="hidden" value="532" />
         <input class="dlimg dlauximg dlimg2 dlimgts share starttime startleadtime" name="S"
                type="hidden" />
         <input class="dlimg dlauximg dlimg2 dlimgts share startleadtime" name="L"
                type="hidden"
                value="1.5" />
         <input class="dlimg dlauximg share threshold poe bodyClass" name="field" type="hidden"
                value="PoM" />
         <input class="dlimg dlauximg share threshold poe bodyClass" name="var" type="hidden"
                value="Rainfall" />
         <input class="dlimg dlauximg share threshold" name="percentile" type="hidden"
                value="50." />
         <input class="dlimg dlauximg share threshold" name="threshold2" type="hidden" />
         <input class="pickarea dlimgts" name="resolution" type="hidden"
                value="irids:SOURCES:Chile:DMC:Forecasts:Precipitation:ds" />
         
         
      </form>
      
      
      
      <div class="controlBar">
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Forecasts/" shape="rect">Pronósticos</a>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"><span property="term:label">Pronóstico Estacional</span></legend>
            
            
         </fieldset> 
         
         
         
         
         
         <fieldset class="navitem">
            
            
            <legend>Análisis</legend>
            <select class="pageformcopy" name="field">
               <option value="PoM">Porcentaje de la mediana</option>
               <option value="Precipitation">Precipitación</option>
               <option value="Anomaly">Anomalía</option>
            </select>         
            
         </fieldset>
         
         
         <fieldset class="navitem">
            
            <legend>Selección de Estación</legend>	
            
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/(irids%3ASOURCES%3AChile%3ADMC%3AForecasts%3APrecipitation%3Ads)//resolution/parameter/geoobject/(bb%3A-80%3A-55%3A-67%3A-17%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
            
         </fieldset> 
         
         
         
         
         
      </div>
      
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         
         <fieldset class="regionwithinbbox dlimage ver4" about="">
            <img class="dlimgloc"
                 src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-85%2C-56%2C-65%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-71:-34:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+//plotborder+0.1+psdef//plotaxislength+120+psdef+.gif" />
            
            
            <div>
               <a class="dlimgts" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                  shape="rect"></a>
               
               
               <div class="template">
                  Observaciones para <span class="bold iridl:long_name"></span>
                  
                  
               </div>
               
               
            </div>
            
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/.InputObs/ESTID/%28irids:SOURCES:Chile:DMC:Forecasts:Precipitation:Forecast:ESTID%40Pudahuel:ds%29//region/parameter/geoobject/.ESTID/.first/VALUE/dup/%5BT2%5Daverage/exch/%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a:/percentile/0.05/VALUE/percentile/removeGRID//fullname/%28M%C3%ADnimo%20Esperado%29/def/:a:/percentile/0.5/VALUE/percentile/removeGRID//fullname/%28Normal%29/def/:a:/percentile/0.95/VALUE/percentile/removeGRID//fullname/%28M%C3%A1ximo%20Esperado%29/def/:a/2/index/0.0/mul/dup/6/-1/roll/exch/6/-2/roll/5/index//long_name/%28Precipitaci%C3%B3n%20Normal%20y%20Pronosticada%29/def//units/%28mm%29/def/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/.Forecast//fullname/%28Forecast%29/def/ESTID/%28irids:SOURCES:Chile:DMC:Forecasts:Precipitation:Forecast:ESTID%40Pudahuel:ds%29//region/parameter/geoobject/.ESTID/.first/VALUE/S/%28T%29/renameGRID/T/last/dup/18/sub/exch/RANGE//fullname/%28Precipitaci%C3%B3n%20Pronosticada%29/def/DATA/0/AUTO/RANGE/6/5/roll/pop/7/-6/roll/6/array/astore/%7B/T//pointwidth/1/def/pop/T/0.5/shiftGRID/%5BT%5DregridLinear%7Dforall/7/-1/roll/5/-4/roll/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//framelabel/%28Precipitaci%C3%B3n%20Normal%20y%20Pron%C3%B3sticada%20para%20los%20pr%C3%B3ximos%203%20meses%29/psdef//plotborderbottom/40/psdef//antialias/true/def//fntsze/13/psdef//plotaxislength/520/psdef/ESTID/last/plotvalue/L/last/plotvalue+.gif" />
               
               
            </div>
            
            
            
         </fieldset>
         
         
         
         <fieldset class="dlimage">
            <a rel="iridl:hasFigure"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/lon/lat/2/copy/%28Precipitation%29//field/parameter/%28Precipitation%29eq/%7BForecast/precip_colors%7Dif//field/get_parameter/%28Anomaly%29eq/%7BForecast/CrossValHind%5BT2%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29eq/%7BForecast/CrossValHind%5BT2%5D0.5/0.0/replacebypercentile/div/%28percent%29unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian%7Dif/6/-1/roll/.target_date/X/Y/fig-/colors/||/black/scatter/scattercolor/plotlabel/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//plotaxislength/520/psdef//antialias/true/psdef//fntsze/16/psdef//S/last/plotvalue//T/S/last/cvsunits//S/parameter/7/3/getinterval/interp/nip/plotvalue//L/first/plotvalue//plotborderbottom/72/psdef/Y/-56.0/-16.0/plotrange//Forecast/0/371.5328/plotrange//plotborder/72/psdef//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Btarget_date%5D%29psdef/"
               shape="rect"></a>
            <img class="dlimg" rel="iridl:hasFigureImage"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/lon/lat/2/copy/%28Precipitation%29//field/parameter/%28Precipitation%29eq/%7BForecast/precip_colors%7Dif//field/get_parameter/%28Anomaly%29eq/%7BForecast/CrossValHind%5BT2%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29eq/%7BForecast/CrossValHind%5BT2%5D0.5/0.0/replacebypercentile/div/%28percent%29unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian%7Dif/6/-1/roll/.target_date/X/Y/fig-/colors/||/black/scatter/scattercolor/plotlabel/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//plotaxislength/520/psdef//antialias/true/psdef//fntsze/16/psdef//S/last/plotvalue//T/S/last/cvsunits//S/parameter/7/3/getinterval/interp/nip/plotvalue//L/first/plotvalue//plotborderbottom/72/psdef/Y/-56.0/-16.0/plotrange//Forecast/0/371.5328/plotrange//plotborder/72/psdef//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Btarget_date%5D%29psdef/+.gif"
                 border="0" />
            <img class="dlauximg" rel="iridl:hasFigureImage"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/lon/lat/2/copy/%28Precipitation%29//field/parameter/%28Precipitation%29eq/%7BForecast/precip_colors%7Dif//field/get_parameter/%28Anomaly%29eq/%7BForecast/CrossValHind%5BT2%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29eq/%7BForecast/CrossValHind%5BT2%5D0.5/0.0/replacebypercentile/div/%28percent%29unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian%7Dif/6/-1/roll/.target_date/X/Y/fig-/colors/||/black/scatter/scattercolor/plotlabel/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//plotaxislength/520/psdef//antialias/true/psdef//fntsze/20/psdef//S/last/plotvalue//T/S/last/cvsunits//S/parameter/7/3/getinterval/interp/nip/plotvalue//L/first/plotvalue//plotborderbottom/72/psdef/Y/-56.0/-16.0/plotrange//Forecast/0/371.5328/plotrange//plotborder/72/psdef//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Btarget_date%5D%29psdef/+.auxfig/+.gif" />
            
            
         </fieldset>
         
         
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h2 align="center" property="term:title">Pronóstico Estacional local - Precipitación DMC</h2>
            
            
            <p align="left" property="term:description">
               Este mapa muestra los pronósticos de la precipitación para Chile.
               
            </p>
            
            
            <p>Los pronósticos de precipitación permiten anticipar eventos de déficit o superávit de la precipitación con algunos meses de
               anticipación.
               
            </p>
            
            
            <p>Selecciona en el menú&gt;análisis el análisis de preferencia: precipitación, anomalía, porcentaje de la mediana, probabilidad
               de excedencia y probabilidad de non-excedencia. En el menú&gt;región puedes seleccionar la región de interés.
               
            </p>
            
             <p><b>Porcentaje de la mediana:</b>
               Este mapa muestra el pronóstico como porcentaje de la mediana. La mediana es el valor de la observación central de una serie
               de datos ordenados.
               
            </p>
           
            <p><b>Precipitación:</b>
               El mapa muestra el pronóstico de precipitación para diferentes puntos en Chile. 
               
            </p>
            
            
            <p><b>Anomalía:</b>
               Este mapa muestra el pronóstico de precipitación como anomalía. La anomalía indica la desviación de la precipitación pronosticado
               comparado con el promedio. Valores negativos indican que el pronóstico de la precipitación es menor que el promedio. Valores
               positivas indican que el pronóstico de la precipitación es mayor que el promedio. 
               
            </p>
            
            

              
            
            <p align="left">Pronóstico proporcionado por<img src="../../icons/Logo_dmc" alt="Logo DMC" /></p>
            
            
         </div>
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            <h2 align="center">¿Cómo se calcula el pronóstico?</h2>
            
            
            <p>
               Los pronósticos están basados en un modelo estadística que relaciona las precipitaciones en las ciudades de Chile con la temperatura
               superficial del mar en el Pacífico ecuatorial para el período 1971-2012.
               
               
            </p>
            
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel" about="">
            
            
            <h2 align="center">Fuente de los Datos</h2>
            
            
            <p><a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/"
                  shape="rect">Pronóstico Estacional</a>, entregado por la Dirección Meteorológica de Chile <a href="http://www.meteochile.gob.cl/" shape="rect">(DMC)</a></p>
            
            
         </div>
         
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            <h3 align="center">Soporte</h3>
            
            
            <p>
               Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile"
                  shape="rect">mwar_lac@unesco.org</a>
               
               
            </p>
            
            
         </div>
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            
            <h3 align="center">Instrucciones</h3>
            
            
            <div class="buttonInstructions"></div>
            
            
         </div>
         
         
      </div>
      
      
      <div class="optionsBar">
         
         
         <fieldset class="navitem" id="share">
            
            <legend>Share</legend>
            
         </fieldset>
         
         
      </div>
      
      
   </body> </html>

