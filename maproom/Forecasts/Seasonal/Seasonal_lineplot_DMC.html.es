<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Forecasts_Seasonal" />
<title>Seasonal Precipitation Forecast</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Seasonal_lineplot_DMC.html?Set-Language=es" />
<link class="share" rel="canonical" href="Seasonal_lineplot_DMC.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/label/use_as_grid/InputObs%5BT2%5D0.33/0.66/0/replacebypercentile//fullname/%28percentile%29def/a:/percentile/last/VALUE/:a:/percentile/first/VALUE/:a/Forecast//fullname/%28Seasonal%20Forecast%29def/target_date/label/fig-/red/dashed/line/red/dashed/line/blue/solid/line/plotlabel/-fig//plotaxislength/700/psdef//S/last/plotvalue//T/S/last/cvsunits//S/parameter/7/3/getinterval/interp/nip/plotvalue//framelabel/%28Prediccion%20de%20%25=%5Btarget_date%5D%20hecha%20en%20%25B%20%25Y%5BS%5D%29psdef/++//T/0.5/plotvalue//S/648.0/plotvalue+//plotaxislength+700+psdef//plotborder+72+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg share" name="bbox" type="hidden" />
<input class="dlimg share" name="S" type="hidden" />
<input class="dlimg dlauximg" name="plotaxislength" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Forecasts/">Pron&#243;sticos</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"><span property="term:label">Pron&#243;stico Estacional</span></legend>
            </fieldset> 
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>
<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/label/use_as_grid/InputObs%5BT2%5D0.33/0.66/0/replacebypercentile//fullname/%28percentil%29def/T/S/last/cvsunits//S/parameter/7/3/getinterval/nip/VALUE/a:/percentile/last/VALUE/DATA/0/AUTO/RANGE/:a:/percentile/first/VALUE/:a/Forecast//fullname/%28Pronostico%20Estacional%29def/target_date/label/fig-/red/dashed/line/red/dashed/line/blue/solid/line/plotlabel/-fig//plotaxislength/970/psdef//framelabel/%28Prediccion%20de%20%25=%5Btarget_date%5D%20hecha%20en%20%25B%20%25Y%5BS%5D%29psdef/%28fntsze%2915/psdef//S/last/plotvalue//plotaxislength/970/psdef//plotborder/72/psdef/" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/label/use_as_grid/InputObs%5BT2%5D0.33/0.66/0/replacebypercentile//fullname/%28percentil%29def/T/S/last/cvsunits//S/parameter/7/3/getinterval/nip/VALUE/a:/percentile/last/VALUE/DATA/0/AUTO/RANGE/:a:/percentile/first/VALUE/:a/Forecast//fullname/%28Pronostico%20Estacional%29def/target_date/label/fig-/red/dashed/line/red/dashed/line/blue/solid/line/plotlabel/-fig//plotaxislength/900/psdef//framelabel/%28Prediccion%20de%20%25=%5Btarget_date%5D%20hecha%20en%20%25B%20%25Y%5BS%5D%29psdef/%28fntsze%2915/psdef//S/last/plotvalue//plotaxislength/970/psdef//plotborder/72/psdef/++//S/last/plotvalue+//plotaxislength+970+psdef//plotborder+72+psdef+.gif"  border="0" alt="image" /><br />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Precipitaci&#243;n</h2>
<p align="left" property="term:description">Este grafico muestra los pron&#243;sticos para las diferentes estaciones meteorol&#243;gicas de la Direcci&#243;n Meteorol&#243;gica de Chile (DMC).</p>
<p align="left">El periodo de pron&#243;stico est&#225; indicado por debajo del gr&#225;fico. </p>
<p align="left">En el grafico los percentiles de 0.66 y 0.33 est&#225;n presentados. El percentil 0.66 indica el valor debajo de lo cual se encuentra 66&#37; de las observaciones hist&#243;ricamente observados para el tiempo en consideraci&#243;n (es decir el periodo de pron&#243;stico).</p>
<p align="left">Pron&#243;stico proporcionado por<img src="../../icons/Logo_dmc" alt="Logo DMC"></img></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula el pron&#243;stico?</h3>
<p>
Los pron&#243;sticos est&#225;n basados en un modelo estad&#237;stico que relaciona las precipitaciones en las ciudades de Chile con la temperatura superficial del mar en el Pac&#237;fico ecuatorial para el per&#237;odo 1971-2012.
 </p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p><a href="http://climatedatalibrary.cl/SOURCES/.Chile/.DMC/.Forecasts/.Precipitation/">Pron&#243;stico</a>, entregado por la Direcci&#243;n Meteorol&#243;gica de Chile<a href="http://www.meteochile.gob.cl/">(DMC)</a></p>
</div>


<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>
 </body>
 </html>
