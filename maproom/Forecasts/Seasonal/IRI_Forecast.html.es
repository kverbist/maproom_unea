<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta xml:lang="es" property="maproom:Entry_Id" content="Forecasts_Seasonal" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>IRI Pron&#243;stico Estacional</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="IRI_Forecast.html?Set-Language=en" />
<link class="share" rel="canonical" href="IRI_Forecast.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-80.0/-67.0/plotrange/Y/-55/-17/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef/+//L/first/plotvalue//F/last/plotvalue//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+590+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts dlimgloc share starttime startleadtime">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc share" name="bbox" type="hidden" />
<input class="dlimg dlimgts share starttime startleadtime" name="F" type="hidden" />
<input class="dlimg dlimgts share startleadtime" name="L" type="hidden" value="1." />
<input class="dlimgnotuseddlauximg" name="plotaxislength" type="hidden" />
<input class="share dlimgts dlimgloc" name="region" type="hidden" />
<input class="pickarea" name="resolution" type="hidden" value=".5" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Forecasts/">Pron&#243;sticos</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"><span property="term:label">Pron&#243;stico Estacional</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
           <fieldset class="navitem valid">
          <legend>Target Time</legend>
	    <link class="starttime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/info.json" />
          <select class="pageformcopy" name="L">
	    <optgroup class="template" label="Target Time">
	    <option class="iridl:values L@value target_date"></option>
	      </optgroup>
	    </select>
  </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>
<span id="content">
    <fieldset class="regionwithinbbox dlimage" about="">
<table>
<tr>
  <td rowspan="2"><img class="dlimgloc" src="http://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-73%2C-32%2C-71%2C-34%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
  </td>

  <td>
    <span class="valid">
       <a class="startleadtime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/L/first/cvsnamedunits//L/parameter/VALUE%5BL/F%5DREORDER/info.json" ></a>
      <span class="template">
        <table bgcolor="f0f0f0">
           <tr>
              <td>Target Date</td><td>Lead Time</td><td>Issue Date</td>
           </tr>
           <tr>
             <th class="iridl:value"></th>
             <th class="iridl:hasIndependentVariables">
               <span class="iridl:value"></span>
             </th>
           </tr>
         </table>
       </span>
     </span>
   </td>
</tr>
</table>

   <br />
    <div  style="display: inline-block; width:49%">
      <h4>Precipitaci&#243;n</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/a:/.prob/Y/low/high/RANGE/dup/Y/0.5/boxAverage/X/0.5/boxAverage/%28bb:9:19:9.5:19.5:bb%29//region/parameter/geoobject/rasterize%5BX/Y%5DregridAverage/0/flaggt/exch/1/index%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20issued%20%25=%5BF%5D%29psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
      </div>
      <div style="display: inline-block; width:49%">
	<h4>Temperatura</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Temperature/a:/.prob/dup/%28bb:-64:-12:-63:-11:bb%29//region/parameter/geoobject/rasterize/exch/1/index%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20issued%20%25=%5BF%5D%29psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
	</div>
  </fieldset>
<fieldset class="dlimage" about="">
<link rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-80.0/-67.0/plotrange/Y/-55/-17/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/" />
<img class="dlimg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-80.0/-67.0/plotrange/Y/-55/-17/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef/+//L/first/plotvalue//F/last/plotvalue//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+590+psdef+.gif" border="0" alt="image" /><br />
<img class="dlauximg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-80.0/-67.0/plotrange/Y/-55/-17/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef/.auxfig+//L/first/plotvalue//F/last/plotvalue//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+590+psdef+.gif" />
</fieldset>
</span>
<div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Pron&#243;stico Estacional Global (IRI) </h2>
<p align="left" property="term:description">Este mapa muestra el pron&#243;stico estacional para precipitaciones y temperaturas en Chile, e indica el escenario m&#225;s probable para la pr&#243;xima temporada, expresado como m&#225;s alto, normal y m&#225;s bajo que lo normal.</p>
<p>Los pron&#243;sticos son para periodos de tres meses. En el men&#250;>target time, puedes elegir el periodo de inter&#233;s. En el men&#250;>regi&#243;n puedes seleccionar la regi&#243;n de inter&#233;s.</p>
<p align="left">Pron&#243;stico proporcionado por<img src="../../icons/Logo_iri" alt="Logo IRI"></img></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h2  align="center">&#191;C&#243;mo se calcula el pron&#243;stico?</h2>
<p>El m&#233;todo del IRI est&#225; basado en el uso de m&#250;ltiples modelos de predicci&#243;n para la precipitaci&#243;n, basado en estimaciones de temperatura del mar.
 </p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Fuente de los Datos</h2>
<p><a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.PiC/.pic3mo_same/Y/-85/85/RANGE/X/-180/180/RANGEEDGES/">Pron&#243;stico Estacional Global</a>, entregado por el <a href="http://iri.columbia.edu/">IRI</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
