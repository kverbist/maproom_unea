<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Pronosticos Estacionales</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
<link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28%25=%5Btarget_date%5D%20%20issued%20%25=%5BF%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/639.5/plotvalue/X/-80.0/-67.0/plotrange/Y/-55/-17/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef/++//L/4.0/plotvalue//F/578.5/plotvalue//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+590+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio Agroclim&#225;tico</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                     <span class="navtext">Pron&#243;sticos</span>
            </fieldset> 
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Pron&#243;sticos</h2>
<p align="left" property="term:description">En este maproom se visualiza los pronosticos a corto y mediano plazo de modelos internacionales y pronosticos con mayor detalle local.</p>
<p>Se presentan dos tipos de pron&#243;sticos. A corto plazo se presentan pron&#243;sticos a seis d&#237;as que informan sobre las condiciones meteorol&#243;gicos y hidrol&#243;gicos esperadas.</p> 
<p>Para informar sobre condiciones esperadas a m&#225;s largo plazo, se presentan los pron&#243;sticos estacionales como instrumentos relevantes para anticipar riesgos climaticos futuros. Los pron&#243;sticos estacionales fueron realizados usando el IRI Climate Prediction Tool (CPT).
</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Forecasts/" >
 <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_SixDayForecast_term" /> 
 <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
