<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#"
      version="XHTML+RDFa 1.0">
   <head>
      <title>Pronóstico de Temperatura Máxima</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en"
            href="MaxTemperature_Princeton.html?Set-Language=en" />
      <link rel="canonical" href="MaxTemperature_Princeton.html" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_SixDayForecast_term" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon"
            href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%28.dia1%29//var/parameter/dup/interp/SOURCES/.Chile/.UPrinceton/.CountryMask/.mask/mul//long_name/%28Pronostico%20de%20Temperatura%29/def/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/X/Y/fig-/colors/black/thin/countries_gaz/-fig/%28antialias%29/true/psdef//framelabel/%28Fecha:%20%25=%5BForecastDate%5D%29/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef//framelabel+%28Fecha:%20%25=%5BForecastDate%5D%29+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body xml:lang="es">
      
      
      
      <form name="pageform" id="pageform">
         <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
         <input class="carryup carry dlimg share localemap" name="bbox" type="hidden" />
         <input class="dlimg share" name="S" type="hidden" />
         <input class="dlimg dlauximg onlyvar share" name="var" type="hidden"
                data-default="dia1" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
         <input class="share dlimgloc dlimgts" name="region" type="hidden" />
         
         
         
      </form>
      
      
      
      <div class="controlBar">
         
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            
            <legend>Chile</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Forecasts/">Pronósticos</a>
            
            
            
         </fieldset> 
         
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_SixDayForecast_term"><span property="term:label">Pronósticos para los Próximos 6 D&#237;as</span></legend>
            
            
            
         </fieldset> 
         
         
         
         <fieldset class="navitem">
            
            
            
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               		
               
               
               <option value="dia1">¿Cual es el pronóstico para mañana?</option>
               		
               
               
               <option value="dia2">¿Cual es el pronóstico para pasado mañana?</option>
               		
               
               
               <option value="dia3">¿Cual es el pronóstico para tres d&#237;as más?</option>
               		
               
               
               <option value="dia4">¿Cual es el pronóstico para cuatro d&#237;as más?</option>
               		</select>
            
            
            
         </fieldset>
         
         
         
         <fieldset class="navitem"> 
            
            
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"></a>
            <select class="RegionMenu" name="bbox">
               
               
               
               <option value="">Chile</option>
               
               
               
               <optgroup class="template" label="Region">
                  
                  
                  
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                  
                  
                  
               </optgroup>
               </select>
            
            
            
         </fieldset>
         	     
         
         
         <fieldset class="navitem">
            
            
            <legend>Promedio espacial sobre </legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               	     
               
               
               <option value="0.125">ubicación puntual</option>
               	     
               
               
               <option value="irids:SOURCES:Features:Political:Chile:regiones:ds">Región</option>
               	     
               
               
               <option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
               	     
               
               
               <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
               	     
               
               
               <option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Distrito</option>
               	     </select>
            	     
            
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               
               
               
               <optgroup class="template" label="Label">
                  
                  
                  
                  <option class="iridl:values region@value label"></option>
                  
                  
                  
               </optgroup>
               </select>
            
            
            
         </fieldset>
         
         
         
         
      </div>
      
      
      
      <div class="ui-tabs">
         
         
         
         <ul class="ui-tabs-nav">
            
            
            
            <div id="ifrcdiv">
               
               
               
            </div>
            
            
            
            <li><a href="#tabs-1">Descripción</a></li>
            
            
            
            <li><a href="#tabs-2">Instructiónes</a></li>
            
            
            
            <li><a href="#tabs-3">Fuente de los Datos</a></li>
            
            
            
            <li><a href="#tabs-4">Contáctenos</a></li>
            
            
            
         </ul>
         
         
         
         
         <fieldset class="dlimage regionwithinbbox">
            
            
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.0166%2C-30.0166%2C-71.0%2C-30.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
               
            </div>	
            
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a> 
                  
                  
                  
                  
                  <div class="template"> Pronósticos para <span class="bold iridl:long_name"></span>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
               
               
            </div>
            
            <br />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%7Bdia1/dia2/dia3/dia4%7Dgrouptogrid//long_name/%28Pron%C3%B3stico%20de%20Temperatura%29/def/M/%28Pronostico%29/renameGRID/%28bb:-72.04375%2C-34.04375%2C-71.03125%2C-33.03125%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/dup/Pronostico/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef//framelabel/%28Pron%C3%B3stico%20de%20temperatura%20para%20los%20pr%C3%B3ximos%204%20d%C3%ADas%29/psdef/ForecastDate/last/plotvalue/T/last/plotvalue+.gif" />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%7Bdia1/dia2/dia3/dia4%7Dgrouptogrid//long_name/%28Pron%C3%B3stico%20de%20Temperatura%29/def/M/%28Pronostico%29/renameGRID/%28bb:-72.04375%2C-34.04375%2C-71.03125%2C-33.03125%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/dup/Pronostico/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef//framelabel/%28Pron%C3%B3stico%20de%20temperatura%20para%20los%20pr%C3%B3ximos%204%20d%C3%ADas%29/psdef/ForecastDate/last/plotvalue/T/last/plotvalue+.auxfig/+.gif" />
            
            
            
            
         </fieldset>
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%28.dia1%29//var/parameter/dup/interp/SOURCES/.Chile/.UPrinceton/.CountryMask/.mask/mul//long_name/%28Pron%C3%B3stico%20de%20Temperatura%29/def/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/X/Y/fig-/colors/black/thin/countries_gaz/-fig/%28antialias%29/true/psdef//framelabel/%28Fecha:%20%25=%5BForecastDate%5D%29/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%28.dia1%29//var/parameter/dup/interp/SOURCES/.Chile/.UPrinceton/.CountryMask/.mask/mul//long_name/%28Pron%C3%B3stico%20de%20Temperatura%29/def/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/X/Y/fig-/colors/black/thin/countries_gaz/-fig/%28antialias%29/true/psdef//framelabel/%28Fecha:%20%25=%5BForecastDate%5D%29/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/ForecastDate/last/plotvalue/T/last/plotvalue+.gif"
                 border="0"
                 alt="image" /><br />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.UPrinceton/.Tmax/.Forecast/%28.dia1%29//var/parameter/dup/interp/SOURCES/.Chile/.UPrinceton/.CountryMask/.mask/mul//long_name/%28Pron%C3%B3stico%20de%20Temperatura%29/def/temp_colors//scale_symmetric/false/def/DATA/AUTO/AUTO/RANGE/X/Y/fig-/colors/black/thin/countries_gaz/-fig/%28antialias%29/true/psdef//framelabel/%28Fecha:%20%25=%5BForecastDate%5D%29/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//fntsze/20/psdef/ForecastDate/last/plotvalue/T/last/plotvalue+.auxfig/+.gif" />
            
            
            
         </fieldset>
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            
            <h2 align="center" property="term:title">Pronóstico de Temperatura Máxima</h2>
            
            
            
            <p align="left" property="term:description">Este mapa muestra la temperatura máxima esperada (en grados C) que se espera para los próximos 4 d&#237;as.
               
               
            </p>
            
            
            
            
         </div>
         
         
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            
            <h2 align="center">Cómo usar este mapa interactivo</h2>
            
            
            
            <p><i>Cambiar a otro mapa:</i>
               Seleccione el menú desplegable que se encuentra en la parte superior de esta página, a la derecha del encabezado azul “Pronósticos
               en contexto”.
               
               
            </p>
            
            
            
            <p><i>Regresar a la página del menú: </i>
               Haga clic en el enlace azul titulado “Pronósticos en contexto” que se encuentra en la esquina superior izquierda de la página.
               
               
            </p>
            
            
            
            <p><i>Amplìe hasta una región:</i><br />
               Método 1: Seleccione una región de la lista:
               
               
               
               <ol>
                  
                  
                  
                  <li>Seleccione el menú desplegable titulado “Región” que se encuentra en la parte superior de la página.</li>
                  
                  
                  
                  <li>Haga clic en la región de interés y el mapa se actualizará automáticamente.</li>
                  
                  
                  
               </ol>
               
               
               
            </p>
            
            
            
            <p>Método 2: Hacer clic y arrastrar</p>
            
            
            
            <ol>
               
               
               
               <li>Haga clic en el botón izquierdo del ratón en la esquina superior izquierda de la región que quiere ampliar.</li>
               
               
               
               <li>Mientras mantiene presionado el botón, arrastre el ratón a la esquina inferior derecha de la región que quiere ampliar.</li>
               
               
               
               <li>Suelte el botón izquierdo del ratón. El mapa se re-dibujará automáticamente.</li>
               
               
               
            </ol>
            
            
            
            <p><i>Aleje el mapa global:</i></p>
            
            
            
            <ol>
               
               
               
               <li>Mueva su ratón sobre el mapa hasta que vea que aparecen tres iconos en la esquina superior izquierda.</li>
               
               
               
               <li>Haga clic en el icono de la lupa.</li>
               
               
               
               <li>El mapa se re-dibujará automáticamente. Note que el mapa no puede mostrar áreas fuera de las latitudes mostradas inicialmente
                  en la interfaz (es decir 66.25°S - 76.25°N).
                  
                  
               </li>
               
               
               
            </ol>
            
            
            
            <p><i>Cambie la fecha del pronóstico: </i>
               Los pronósticos se rotulan por el dìa en que se publicaron. Puede encontrar este rótulo moviendo su ratón sobre el mapa hasta
               que en la parte superior aparezca un recuadro de texto que contiene datos.
               
               
               
               <ol>
                  
                  
                  
                  <li>Para moverse hacia adelante o atrás un dìa, haga clic sobre los botones correspondientes a la izquierda o a la derecha del
                     recuadro de texto y el mapa se actualizará automáticamente.
                     
                     
                  </li>
                  
                  
                  
                  <li>Para cambiar manualmente la fecha de publicación del pronóstico, introduzca su fecha de interés en el recuadro de texto. éste
                     debe tener el siguiente formato: “0000 16 Ene 2008”. Luego presione “intro” o haga clic en el icono “actualizar” que se encuentra
                     en la esquina superior izquierda del mapa.
                     
                     
                  </li>
                  
                  
                  
                  <li>Para crear una animación de estos mapas a lo largo de una serie de fechas, introduzca el rango de fechas en el recuadro de
                     texto. éste debe tener el siguiente formato: “fecha de inicio” seguida de “al” seguida de “fecha de finalización”. Por ejemplo
                     “0000 1 Jan 2008 al 0000 15 Jan 2008”.
                     
                     
                  </li>
                  
                  
                  
               </ol>
               
               
               
            </p>
            
            
            
            <div class="buttonInstructions"></div>
            
            
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            
            
            <h2 align="center">Documentación</h2>
            
            
            
            <p><b><a class="carry"
                     href="http://stream.princeton.edu/LAFDM/WEBPAGE/interface.php?locale=sp">Monitor de Inundaciones y Sequ&#237;a de la Universidad de Princeton: </a></b>Mapas de Pronóstico de Temperatura Máxima Diaria<br />
               <b>Datos</b>  Promedios de temperatura máxima diaria pronosticada a 0.25° lat/lon con el modelo NCEP Global Ensemble Forecasting System
               (GEFS) corrido diariamente a las 00 UTC por el NOAA ESRL PSD Reforecast-2 project 
               <br /><b>Fuente de Datos</b> U. S. National Oceanic and Atmospheric Administration (NOAA), Earth System Research Laboratory (ESRL), Physical Sciences
               Division (PSD), <a href="http://esrl.noaa.gov/psd/forecasts/reforecast2/">Proyecto de ESRL Reforecast-2</a>
               <br /><b>Análisis</b>: Los análisis presentados aqu&#237; incluyen pronósticos de las temperaturas máximas diarias. 
               
               
               
            </p>
            
            
            
         </div>
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            
            <h2 align="center">Servicios de Asistencia</h2>
            
            
            
            <p>
               En contacto con <a href="mailto:mwar-lac@unesco.org?subject=Precipitation Forecast in Context Map Tool">mwar-lac@unesco.org</a> con cualquier pregunta técnica o problemas con esta Sala de Mapas, por ejemplo, las previsiones no mostrar o actualizar correctamente.
               
               
               
            </p>
            
            
            
         </div>
         
         
         
      </div>
      
      
      
      <div class="optionsBar">
         
         
         
         <fieldset class="navitem" id="share">
            
            
            <legend>Compartir</legend>
            
            
         </fieldset>
         
         
         
         <fieldset class="navitem langgroup" id="contactus"></fieldset>
         
         
         
      </div>
      
      
      
   </body>
</html>