<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#"
      version="XHTML+RDFa 1.0">
   <head>
<title>Pron&#243;stico de Precipitaci&#243;n</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="cummrain.html?Set-Language=en" />
<link rel="canonical" href="cummrain.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_SixDayForecast_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcp/-a-/.forecasttime/-a/X/Y/fig-+colors+plotlabel+black+thin+countries_gaz+-fig+/S/last/plotvalue//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel+(Forecast%20for%20%25=%5Bforecasttime%5D%20Issued%20%25=%5BS%5D)+psdef//antialias+true+psdef//color_smoothing+null+psdef//apcp/0/300/plotrange/S/last/plotvalue//plotborder+0+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
<script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform">
<input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
<input class="carryup carry dlimg share localemap" name="bbox" type="hidden" />
<input class="dlimg share" name="S" type="hidden" />
<input class="dlimg dlauximg onlyvar share" name="var" type="hidden" data-default="Amount" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Chile</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Forecasts/">Pron&#243;sticos</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_SixDayForecast_term"><span property="term:label">Pronósticos para los Próximos 6 Días</span></legend>
            </fieldset> 
            <fieldset class="navitem">
               <legend>An&#225;lisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
		<option value="Amount">¿Cuánta lluvia se espera?</option>
		<option value="Anomaly">¿Dónde se esperan lluvias superiores al promedio?</option>
		<option value="Percentage">¿Cómo se compara la cantidad de lluvia pronosticada con la lluvia normal durante este mes?</option>
		<option value="Percentile">¿Dónde se esperan lluvias inusualmente abundantes?</option>
		</select>
            </fieldset>
            <fieldset class="navitem"> 
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
	     <fieldset class="navitem"><legend>Promedio espacial sobre </legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
	     <option value="0.125">ubicaci&#243;n puntual</option>
	     <option value="irids:SOURCES:Features:Political:Chile:regiones:ds">Regi&#243;n</option>
	     <option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
	     <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
	     <option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Distrito</option>
	     </select>
    	     <link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
            <optgroup class="template" label="Label">
            <option class="iridl:values region@value label"></option>
            </optgroup>
            </select>
            </fieldset>

 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<div id="ifrcdiv">
</div>
      <li><a href="#tabs-1" >Descripción</a></li>
      <li><a href="#tabs-2" >Más Información</a></li>
      <li><a href="#tabs-3" >Instructiónes</a></li>
      <li><a href="#tabs-5" >Fuente de los Datos</a></li>
      <li><a href="#tabs-6" >Contáctenos</a></li>
    </ul>

        <fieldset class="dlimage regionwithinbbox">
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.0166%2C-30.0166%2C-71.0%2C-30.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               	
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
                  
                  <div class="template"> Pron&#243;sticos para <span class="bold iridl:long_name"></span>
                     
                  </div>
                  
               </div>
               
                              <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia1/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia2/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia3/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia4/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia5/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/.dia6/(bb%3A-72.04375%2C-44.04375%2C-71.03125%2C-43.03125)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/toi4/6/ds/info.json"
                     shape="rect"></a> 
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td rowspan="1" colspan="1">Pron&#243;stico para </td>
                           <td class="name " rowspan="1" colspan="1">: </td>
                           <td align="right" class="value " rowspan="1" colspan="1"> mm</td>
                        </tr>
                     </table> 
                     
                     
                  </div>
                  
                  
               </div>
               
              
            </div>
            
            <br />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/%7Bdia1/dia2/dia3/dia4/dia5/dia6%7Dgrouptogrid//long_name/%28Pron%C3%B3stico%20de%20Precipitaci%C3%B3n%29def/M/%28Pronostico%29renameGRID/%28bb:-72.04375%2C-44.04375%2C-71.03125%2C-43.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/AUTO/AUTO/RANGE/dup/Pronostico/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef/S/last/plotvalue/+.gif" />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.IRI/.Six_Day_Forecast/.Amount/%7Bdia1/dia2/dia3/dia4/dia5/dia6%7Dgrouptogrid//long_name/%28Pron%C3%B3stico%20de%20Precipitaci%C3%B3n%29/def/M/%28Pronostico%29/renameGRID/%28bb:-72.04375%2C-44.04375%2C-71.03125%2C-43.03125%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//units/%28mm/dia%29/def/DATA/AUTO/AUTO/RANGE/dup/Pronostico/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef/S/last/plotvalue/.auxfig/+.gif" />
           
         </fieldset>

<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/%28Amount%29//var/parameter/%28Amount%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcp//long_name/%28Pron%C3%B3stico%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentage%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctmonthlyclim//long_name/%28Porcentaje%20de%20precipitaci%C3%B3n%20normal%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentile%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctlenoseasonal//long_name/%28Percentil%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcpanom//long_name/%28Anomal%C3%ADa%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif/" />
<img class="dlimg" src="http://iridl.ldeo.columbia.edu/%28Amount%29//var/parameter/%28Amount%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcp//long_name/%28Pron%C3%B3stico%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentage%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctmonthlyclim//long_name/%28Porcentaje%20de%20precipitaci%C3%B3n%20normal%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentile%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctlenoseasonal//long_name/%28Percentil%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcpanom//long_name/%28Anomal%C3%ADa%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif/+.gif" border="0" alt="image" /><br />
<img class="dlauximg" src="http://iridl.ldeo.columbia.edu/%28Amount%29//var/parameter/%28Amount%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcp//long_name/%28Pron%C3%B3stico%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/500/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentage%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctmonthlyclim//long_name/%28Porcentaje%20de%20precipitaci%C3%B3n%20normal%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Percentile%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcppctlenoseasonal//long_name/%28Percentil%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//var/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.MD/.IFRC/.ESRL/.PSD/.reforecast2/.ensemble_mean/.analyses/.tot6day/a-/.apcpanom//long_name/%28Anomal%C3%ADa%20de%20precipitaci%C3%B3n%20para%20los%20pr%C3%B3ximos%206%20d%C3%ADas%29/def/-a-/.forecasttime/-a/X/Y/fig-/colors/plotlabel/black/thin/countries_gaz/-fig/S/last/plotvalue//color_smoothing/null/psdef//X/-85/-65/plotrange//Y/-56/-17/plotrange//framelabel/%28Pron%C3%B3stico%20para%20%25=%5Bforecasttime%5D%29/psdef//antialias/true/psdef//plotaxislength/550/psdef//XOVY/null/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef/%7Dif//fntsze/20/psdef/+.auxfig/+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Pron&#243;stico de Precipitaci&#243;n</h2>
<p align="left" property="term:description">Este mapa muestra la cantidad total de lluvia o nieve (en mm) que se espera que caiga en los próximos 6 días ys e compara con las condiciones normales durante este mes.</p>
<p>In el menu (&#39;An&#225;lisis&#39;) se puede seleccionar los siguientes 4 tipos de an&#225;lisis:</p>
<h4 align="left">1) ¿Cuánta lluvia se espera? </h4>
<p>Muestra el pron&#243;stico de lluvia esperada para los pr&#243;ximos seis d&#237;as.</p>
<h4 align="left">2) ¿Dónde se esperan lluvias superiores al promedio?</h4>
<p>Este mapa muestra dónde se predice que la lluvia/nieve total en los próximos seis días sea más o menos el promedio para esta parte del año.</p>
<h4 align="left">3) ¿Cómo se compara la cantidad de lluvia pronosticada con la lluvia normal durante este mes?</h4>
<p>Este mapa muestra &#225;reas que tienen un pronóstico de recibir en los próximos seis días un grande porcentaje de la lluvia que se recibe normalmente durante este mes. Un color azul m&#225;s obscuro indica precipiactaciones m&#225;s altos con respecto al valor mensual. Un color rojo y violeta indica donde la precipitaci&#243;n en los pr&#243;ximos 6 d&#237;as es probablemente 2 o 3 veces el valor mensual esperado para esta epoca (200%-300%). Colores de azul intermedios indican pron&#243;sticos donde se espera precipitaci&#243;n entre 100% y 150% la cantidad de precipitaci&#243;n esperada para este mes. Precipitaci&#243;n normal o por debajo del normal se muestra en blanco </p>
<h4 align="left">4) ¿Dónde se esperan lluvias inusualmente abundantes?</h4>
<p>Este mapa muestra las áreas donde se predice que la lluvia (o nieve) acumulada en los próximos seis días sea excepcionalmente abundante relativo a la lluvia normal en este locación.</p>
<p>Consulte la sección “Más información” para ver m&#225;s detalles.</p>
</div>
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">1) ¿Cuánta lluvia se espera?</h3>
            
            <p align="left">Las áreas azules indican la cantidad acumulada de lluvia pronosticada para el periodo de seis días.  Las áreas en donde se
               esperan las lluvias más abundantes están coloreadas en tonos cada vez más oscuros de azul, luego rojo y violeta.  El mapa
               indica la fecha de comienzo del periodo de seis días.  El mapa indica solamente la cantidad acumulada de lluvia esperada para
               los próximos seis días, pero no indica si dicha cantidad es normal para el lugar o la época del año. 
            </p>
<h3 align="center">2) ¿Dónde se esperan lluvias superiores al promedio?</h3>
<p align="left">Las áreas azules son áreas donde el total de lluvia prevista para los pr&#243;ximos seis días es mayor que la lluvia promedio para este mismo lugar y época en los años anteriores (1985-2012).  Las áreas marrones son áreas donde el total de lluvia prevista para los seis días es menor que la lluvia promedio para este mismo lugar y época en los años anteriores (1985-2012).  Las áreas en marrón no son necesariamente áreas donde se predicen sequías.  Es decir, el mapa no muestra cuán inusuales son los valores pronosticados para el lugar o época del año. </p>

<h3 align="center">3) ¿Cómo se compara la cantidad de lluvia pronosticada con la lluvia normal durante este mes?</h3>
<p align="left">El mapa muestra la cantidad de la lluvia que se espera en todo un mes caerá en los próximos seis días.  Se pronostican lluvias abundantes en las áreas donde se espera que recibirá una proporción grande de la precipitación acumulada mensual normal en los próximos seis días.  Cuanto más oscuro sea el azul, más lluvia se espera en los próximos seis días.  Los colores rojos y violetas indican áreas donde la lluvia acumulada en los próximos seis días es probable que será de 2 a 3 veces (200%-300%) mayor que el promedio de lluvia mensual para ese lugar y época del año (comparado a 1985-2012).  Azul medio indica áreas donde la lluvia acumulada en los próximos seis días se espera que será de 1 a 1 y 1/2 veces (100%-150%) mayor que el promedio de lluvia mensual para ese lugar y época del año.  Las lluvias normales o menores de lo normal se muestran en blanco.</p>

<h3 align="center">4) ¿Dónde se esperan lluvias inusualmente abundantes?</h3>
<p>Este mapa muestra las áreas donde se predice que la lluvia (o nieve) acumulada en los próximos seis días sea excepcionalmente abundante.</p>
<ul>
<li>El tono de azul más oscuro indica las áreas donde se espera que la lluvia total en el periodo de seis días será excepcionalmente abundante, lo que significa una precipitación en el 1% más alto que la experimentada en este lugar en cualquier época del año en el pasado.</li>
<li>El tono de azul mediano indica una precipitación esperada en el 5% más alto.</li>
<li>El tono de azul pálido indica una precipitación esperada en el 10% más alto que la experimentada en este lugar en el pasado.</li>
</ul>
            
            <p align="left"><i>Este pronóstico solamente muestra lluvias sobre áreas grandes, y no debe ser utilizado para predecir la trayectoria de ciclones,
                  lluvias locales, o como una previsión de inundaciones.</i>  El mapa no distingue áreas donde se espera lluvia, de donde se espera nieve.  Los pronósticos para la cantidad de nieve
               están indicados en términos equivalentes a cantidad de lluvia, por lo que la profundidad real de la nieve sería considerablemente
               más que lo indicado.  Los datos de pronóstico son cortesía del proyecto NOAA ESRL Reforecast-2.
               
            </p>
            
         </div>

<div id="tabs-3"  class="ui-tabs-panel">
<h2  align="center">Cómo usar este mapa interactivo</h2>
<p><i>Cambiar a otro mapa:</i>
Seleccione el menú desplegable que se encuentra en la parte superior de esta página, a la derecha del encabezado azul “Pronósticos en contexto”.</p>
<p><i>Regresar a la página del menú: </i>
Haga clic en el enlace azul titulado “Pronósticos en contexto” que se encuentra en la esquina superior izquierda de la página.</p>
<p><i>Amplíe hasta una región:</i><br />
Método 1: Seleccione una región de la lista:
<ol>
<li>Seleccione el menú desplegable titulado “Región” que se encuentra en la parte superior de la página.</li>
<li>Haga clic en la región de interés y el mapa se actualizará automáticamente.</li>
</ol>
</p>
<p>Método 2: Hacer clic y arrastrar</p>
<ol>
<li>Haga clic en el botón izquierdo del ratón en la esquina superior izquierda de la región que quiere ampliar.</li>
<li>Mientras mantiene presionado el botón, arrastre el ratón a la esquina inferior derecha de la región que quiere ampliar.</li>
<li>Suelte el botón izquierdo del ratón. El mapa se re-dibujará automáticamente.</li>
</ol>
<p><i>Aleje el mapa global:</i></p>
<ol>
<li>Mueva su ratón sobre el mapa hasta que vea que aparecen tres iconos en la esquina superior izquierda.</li>
<li>Haga clic en el icono de la lupa.</li>
<li>El mapa se re-dibujará automáticamente. Note que el mapa no puede mostrar áreas fuera de las latitudes mostradas inicialmente en la interfaz (es decir 66.25°S - 76.25°N).</li>
</ol>
<p><i>Cambie la fecha del pronóstico: </i>
Los pronósticos se rotulan por el día en que se publicaron. Puede encontrar este rótulo moviendo su ratón sobre el mapa hasta que en la parte superior aparezca un recuadro de texto que contiene datos.
<ol>
<li>Para moverse hacia adelante o atrás un día, haga clic sobre los botones correspondientes a la izquierda o a la derecha del recuadro de texto y el mapa se actualizará automáticamente.</li>
<li>Para cambiar manualmente la fecha de publicación del pronóstico, introduzca su fecha de interés en el recuadro de texto. éste debe tener el siguiente formato: “0000 16 Ene 2008”. Luego presione “intro” o haga clic en el icono “actualizar” que se encuentra en la esquina superior izquierda del mapa.</li>
<li>Para crear una animación de estos mapas a lo largo de una serie de fechas, introduzca el rango de fechas en el recuadro de texto. éste debe tener el siguiente formato: “fecha de inicio” seguida de “al” seguida de “fecha de finalización”. Por ejemplo “0000 1 Jan 2008 al 0000 15 Jan 2008”.</li>
</ol>
</p>
<div class="buttonInstructions"></div>
</div>
<div id="tabs-5" class="ui-tabs-panel">
<h2  align="center">Dataset de Documentación</h2>
<p><b><a class="carry" href="instructions.html.es#esrlgfs">ESRL GEFS Mapas de Pronóstico de Precipitaciones Diarias y de Seis Días</a></b><br />
<b>Datos</b>  Promedios de lluvia diaria total pronosticada a 1.0° lat/lon con ensambles del modelo NCEP Global Ensemble Forecasting System (GEFS) corrido diariamente a las 00 UTC por el NOAA ESRL PSD Reforecast-2 project 
<br /><b>Fuente de Datos</b> U. S. National Oceanic and Atmospheric Administration (NOAA), Earth System Research Laboratory (ESRL), Physical Sciences Division (PSD), <a href="http://esrl.noaa.gov/psd/forecasts/reforecast2/">Proyecto de ESRL Reforecast-2</a>
<br /><b>Análisis</b>: Los análisis presentados aquí incluyen pronósticos de las precipitaciones diarias (dias 1 a 6) y de las precipitaciones totales en seis días, pronósticos de las anomalías de las precipitaciones diarias y de las anomalías de la precipitación total en seis días (con respecto a 1985-2012), pronóstico de la precipitación diaria y total de seis días como porcentaje de la precipitación total mensual (periodo base 1985-2012), y pronóstico de la precipitación diaria y del total de seis días como percentiles del período base 1985-2012.
</p>
</div>
<div id="tabs-6"  class="ui-tabs-panel">
<h2  align="center">Servicios de Asistencia</h2>
<p>
En contacto con <a href="mailto:mwar-lac@unesco.org?subject=Precipitation Forecast in Context Map Tool">mwar-lac@unesco.org</a> con cualquier pregunta técnica o problemas con esta Sala de Mapas, por ejemplo, las previsiones no mostrar o actualizar correctamente.
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>
