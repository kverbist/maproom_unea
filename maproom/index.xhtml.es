<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Observatorio Agroclim&#225;tico</title>
<link rel="stylesheet" type="text/css" href="unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json"
            href="./navmenu.json" />
      <link rel="shortcut icon" href="./icons/Logo_Minagri_32.png" />
      <link rel="apple-touch-icon" sizes="54x54" href="./icons/Logo_Minagri.png" />
      <link rel="icon" href="./icons/Logo_Minagri.svg" sizes="any" type="image/svg+xml" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
<script type="text/javascript" src="../uicore/uicore.js"></script>
<script type="text/javascript" src="../maproom/unesco.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-24207365-2', 'climatedatalibrary.cl');
  ga('send', 'pageview');

</script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage" name="Set-Language" type="hidden" />
<input class="titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem">
                <legend>Chile</legend> 
                      <a rev="section" class="navlink carryup" href="http://www.climatedatalibrary.cl/">Biblioteca de Datos Climáticos</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                     <span class="navtext">Observatorio Agroclim&#225;tico</span>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Observatorio Agroclim&#225;tico</h2>
<p align="left" property="term:description">
El Observatorio Agroclim&#225;tico consiste de un set de mapas y figuras que muestran las condiciones de sequ&#237;a actuales, y entregan informaci&#243;n sobre la frecuencia de sequ&#237;as pasadas y proyeciones de condiciones clim&#225;ticas futuras.</p>
<p align="left">Un manual que describe todos los variables y opciones se encuentra <a href="Manual Observatorio Agroclimatico_esp.pdf">aqu&#237;</a>.</p>
<p align="left">Un tutorial con un caso de estudio para la Regi&#243;n de O'Higgins en Chile se encuentra <a href="Tutorial Observatorio Agroclimatico_esp.pdf">aqu&#237;</a>.</p>
</div>

<div class="rightcol tabbedentries" about="/maproom/" >
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
