<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring_Meteorological" />
<title>Temperatura Observada RAN</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="Temperature_RAN.html?Set-Language=en" />
<link class="share" rel="canonical" href="Temperature_RAN.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Precipitation/.Monthly/lon/lat/2/copy/Monthly_precip/yearly-anomalies%5BT%5Dstandardize/deltaWASP_colors//plotlast/3/def//plotfirst/-3/def/6/-1/roll/pop//long_name/%28Standardized%20Precipitation%20Anomaly%29def//name/%28st_precip%29def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef/++//T/642.5/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56.04167/-15.95833/plotrange+//plotaxislength+432+psdef//XOVY+null+psdef//plotborder+72+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
<style>
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style>
</head>
<body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share"
            method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="dlimg dlimgloc admin maptable  share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgts dlimgloc dlimglocclick station" name="region"
                type="hidden" />
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="share dlimg dlauximg dlimgts" name="var" type="hidden"
                data-default=".MinTemp" />
         <input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />
         <input class="pickarea dlimgts admin" name="resolution" type="hidden"
                value="irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:ds" />
         
         <!-- list of layers form with names corresponding to different layers of the image so that they can be un/checked by default. I leave it commented out since as of now scatterlabel is not considered a layer
<input class="dlimg share" name="layers" value="Discharge" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="aprod" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="label" checked="unchecked" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" type="checkbox" checked="checked" />
-->
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequ&#237;a Meteorol&#243;gica</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         
         
         <fieldset class="navitem">
            <legend>An&#225;lisis</legend>
            <select class="pageformcopy" name="anal">
               <option value="">Mediciones</option>
               <option value="Anomaly">Anomal&#237;a</option>
               <option value="Percentage">Porcentaje</option></select>
            
         </fieldset>
         
         
         <fieldset class="navitem">
            <legend>Variable</legend>
            <select class="pageformcopy" name="var">
               <option value=".MinTemp">Temperatura M&#237;nima</option>
               <option value=".MaxTemp">Temperatura M&#225;xima</option></select>
            
         </fieldset>
         
         
         <!-- Menu generated by json to list stations according to their variable 'label'. The function labelgeoIdinteresects is directly defined in the link. When you have a more recent version of ingrid, you will be able to take it out. The function has 2 inputs: a resolution in the same format as resolution form/parameter and bbox (optional: by default the World) that constrains to list only labels that fall into bbox, also same format as bbox form/parameter. You may want to adjust default bbox so that it is consistent to the whole Maproom. The rest is fixed. The link must have the class admin (or you can name it otherwise but use same name in the forms declaration. -->
         
         
         <fieldset class="navitem">
            
            <legend>Selecci&#243;n de Estaci&#243;n</legend>	
            
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/(irids%3ASOURCES%3AChile%3ARAN%3ATemperature%3AMinTemp%3AMonthly%3Ads)//resolution/parameter/geoobject/(bb%3A-80%3A-55%3A-67%3A-17%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
            
         </fieldset> 
         
         
         
         
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
         </ul>
         
         
         
         <fieldset class="regionwithinbbox dlimage" about="">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp/exch/pop//long_name/%28Observed%20Temperature%29def/dup/yearly-climatology//long_name/%28Normal%20Average%20Temperature%29def/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_percentage/exch/pop/%28percent%29unitconvert/dup/dataflag/0.0/maskle/exch/100.0/min/mul//long_name/%28Temperature%20Percentage%29def/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_st_anomaly/exch/pop//long_name/%28Temperature%20Anomaly%29def/3/array/astore/%7Bnombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T/5/-4/roll/table:/5/:table/"
               shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-80:-56:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
            </div>
            
            
            <div style="float: left;">
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28pt:-80:-56:pt%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  <div class="template" style="color : black">
                     Estaci&#243;n RAN: <b><span class="iridl:long_name"></span></b>
                     
                  </div>
                  
               </div>
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.RAN/.Precipitation/.Monthly/nombre/%28irids:SOURCES:Chile:RAN:Precipitation:Monthly:nombre%40Vicuna:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
                  
               </div>
               
               <div class="template"> <b>Observaciones para el mes actual: </b></div> 
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.RAN/.Temperature/(.MinTemp)//var/parameter/dup/interp/.Monthly/.Temp/exch/pop/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ARAN%3ATemperature%3AMinTemp%3AMonthly%3Anombre%40Melipilla%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE/toreal//long_name/(Temperatura%20Observada%20(grados%20C)%3A%20)/def/SOURCES/.Chile/.RAN/.Temperature/(.MinTemp)//var/parameter/dup/interp/.Monthly/.Temp_st_anomaly/exch/pop/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ARAN%3ATemperature%3AMinTemp%3AMonthly%3Anombre%40Melipilla%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Anomal%C3%ADa%20de%20la%20Temperatura%20(-)%3A%20)/def/SOURCES/.Chile/.RAN/.Temperature/(.MinTemp)//var/parameter/dup/interp/.Monthly/.Temp_percentage/exch/pop/(percent)/unitconvert/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ARAN%3ATemperature%3AMinTemp%3AMonthly%3Anombre%40Melipilla%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE/toi4//long_name/(Temperatura%20-%20Porcentaje%20del%20Promedio%20(%25)%3A%20)/def/3/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
                  
               </div>
               
            </div>
            
            <br clear="none" />
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage"
                    src="http://www.climatedatalibrary.cl/expert/%28Observed%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_st_anomaly/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//name/%28st_temp%29/def//long_name/%28Anomal%C3%ADa%20de%20la%20Temperatura%20Estandarizada%29/def/pdsi_colorbars/DATA/-3/3/RANGE/dup/T/fig-/colorbars2/-fig/%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Temperatura%20Promedio%20Mensual%29/def/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Percentage%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_percentage/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/51/max/149/min//long_name/%28Temperatura%20-%20%20Porcentaje%20del%20Normal%29/def/pdsi_colorbars/DATA/50.0/150.0/RANGE/dup/T/fig-/colorbars2/-fig/%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/nombre/last/plotvalue/+.gif" />
             
            </div>
            <div class="dlimgtsbox">
               
               <img class="dlauximg regionwithinbbox" rel="iridl:hasFigureImage"
                    src="http://www.climatedatalibrary.cl/expert/%28Observed%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_st_anomaly/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//name/%28st_temp%29/def//long_name/%28Anomal%C3%ADa%20de%20la%20Temperatura%20Estandarizada%29/def/pdsi_colorbars/DATA/-3/3/RANGE/dup/T/fig-/colorbars2/-fig/%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Temperatura%20Promedio%20Mensual%29/def/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Percentage%29/eq/%7B/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/.Temp_percentage/exch/pop/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:RAN:Temperature:MinTemp:Monthly:nombre%40Melipilla:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/51/max/149/min//long_name/%28Temperatura%20-%20%20Porcentaje%20del%20Normal%29/def/pdsi_colorbars/DATA/50.0/150.0/RANGE/dup/T/fig-/colorbars2/-fig/%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/nombre/last/plotvalue/+.auxfig/+.gif" />
               
            </div>
            
           
         </fieldset>
         
         
         <fieldset class="dlimage" id="content" about="">
           
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/%28Observed%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_st_anomaly/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Anomal%C3%ADa%20de%20la%20Temperatura%20Estandarizada%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Temperatura%20Promedio%20Mensual%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_percentage/100.0/min/6/-1/roll/pop/6/-1/roll/pop/pdsi_colorbars/DATA/0.0/100.0/RANGE//long_name/%28Temperatura%20-%20%20Porcentaje%20del%20Normal%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/%28Observed%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_st_anomaly/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Anomal%C3%ADa%20de%20la%20Temperatura%20Estandarizada%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Temperatura%20Promedio%20Mensual%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_percentage/100.0/min/6/-1/roll/pop/6/-1/roll/pop/pdsi_colorbars/DATA/0.0/100.0/RANGE//long_name/%28Temperatura%20-%20%20Porcentaje%20del%20Normal%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif"
                 border="0"
                 alt="image" /><br clear="none" />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/expert/%28Observed%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_st_anomaly/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Anomal%C3%ADa%20de%20la%20Temperatura%20Estandarizada%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp/6/-1/roll/pop/6/-1/roll/pop//long_name/%28Temperatura%20Promedio%20Mensual%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.RAN/.Temperature/%28.MinTemp%29//var/parameter/dup/interp/.Monthly/lon/lat/2/copy/Temp_percentage/100.0/min/6/-1/roll/pop/6/-1/roll/pop/pdsi_colorbars/DATA/0.0/100.0/RANGE//long_name/%28Temperatura%20-%20%20Porcentaje%20del%20Normal%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >Temperatura Observada - RAN</h3>
<p align="left" property="term:description">Este mapa muestra la temperatura observada en la Red Agroclim&#225;tica Nacional (RAN).</p>
<p align="left">Seleccione mediciones, anomal&#237;a o porcentaje en el men&#250;>an&#225;lisis. En men&#250;> regi&#243;n puedes seleccionar la regi&#243;n de inter&#233;s. Utiliza el navegador de tiempo para consultar el mes de inter&#233;s.</p>
<p align="left"><b>Mediciones:</b>
Este mapa muestra la temperatura en diferentes estaciones de la RAN. Las observaciones tienen unidad [grados Celsius].</p>
<p align="left"><b>Porcentaje:</b>
Este mapa muestra la temperatura como porcentaje de la temperatura normalmente esperada en cada mes. El porcentaje indica si hay temperaturas por debajo o por encima de la temperatura normal.</p>
<p align="left"><b>Anomal&#237;a estandarizada:</b>
El mapa muestra la temperatura observada como anomal&#237;a estandarizada. La anomal&#237;a estandarizada es la diferencia entre la temperatura observada en un mes espec&#237;fico y la temperatura observada normalmente en el mismo mes, y permite identificar condiciones extremos con respecto a lo normal.</p> 
<p align="left">Los datos usados para el c&#225;lculo provienen de la Red Agroclim&#225;tica Nacional (RAN). Para mayor informaci&#243;n sobre esta Red consultar en <a href="mailto:unea@minagri.gob.cl?subject=RAN Chile">unea@minagri.gob.cl</a></p>
<p align="left"><a href="http://www.agromet.cl/"><img src="../../icons/Logo_RAN" alt="Logo RAN"></img></a></p>
</div>
            
                  
         
<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula el porcentaje?</h3>
<p>
El porcentaje se calcula dividiendo la precipitaci&#243;n observada en un mes con la precipitaci&#243;n normalmente esperada en este mes (promedio) multiplicado con cien. </p>
</div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <dl class="datasetdocumentation">
               <dt>Data</dt>
               <dd>Temperatura observada <a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.RAN/.Temperature/.MinTemp/.Monthly/"
                     shape="rect">Estaciones RAN</a></dd>
               <dt>Data Source</dt>
               <dd>RAN, <a href="http://www.agromet.cl/" shape="rect">RAN</a></dd>
            </dl>
            
         </div>
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Temperature Chile" shape="rect">mwar_lac@unesco.org</a> with any technical questions or problems with this Map Room.
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>

 </html>
