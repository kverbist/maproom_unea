<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>&#205;ndice de Precipitaci&#243;n Estandardizado Nacional</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="SPI-CAZALAC.html?Set-Language=en" />
<link class="share" rel="canonical" href="SPI-CAZALAC.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_tXXXerm"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/%5B/%28.%29/%28SPI12%29//var/parameter/%5Dconcat/interp/name//long_name/exch/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/X/-75.65/-66.55/plotrange/Y/-55.9/-17.7/plotrange//T/last/plotvalue//color_smoothing/null/psdef//antialias/true/psdef//color_smoothing/1/psdef//plotborder/0/psdef//plotaxislength/800/psdef//antialias/true/psdef+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+733+psdef//antialias+true+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share"
            method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="dlimg admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="carry share dlimgloc dlimgts" name="region" type="hidden" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequía Meteorológica</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="SPI1">IPE 1 Mes</option>
               <option value="SPI3">IPE 3 Meses</option>
               <option value="SPI6">IPE 6 Meses</option>
               <option value="">IPE 12 Meses</option>
		 <option value="SPI24">IPE 24 Meses</option>
		 <option value="SPI36">IPE 36 Meses</option>
               <option value="SPI48">IPE 48 Meses</option>
            </select>
            
         </fieldset>	
         	     
         <fieldset class="navitem">
            <legend>Promedio espacial sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.1">Ubicación puntual</option>
               <option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
               <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
               <option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Distrito</option></select>
            		
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            	<select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI%5B%28.%29%28SPI12%29//var/parameter%5Dconcat/interp//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29def/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.5%2C-70%2C-30.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//units/%28-%29def/T/exch/table-/text/text/skipanyNaN/-table/"
               shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/.SPI1/T/%28Dec%202014%29/VALUE/-50/flaggt/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef/T/last/plotvalue+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               	
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  <div class="template"> Observacioness para <span class="bold iridl:long_name"></span>
                     
                  </div>
                  
               </div>
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  <div>Valores del IPE para el mes actual: </div> 
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/%7B%7D/%7B/(bb%3A-70.5%2C-30.5%2C-70%2C-30.0)//region/parameter/geoobject%5BX/Y%5Dweighted-average/T/last/VALUE/name/cvntos/(SPI)/search/pop/pop/pop//long_name/exch/def%7Dforalldatasets2/%7Ba1/a4/a5/a2/a6/a7/a3%7Dds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td rowspan="1" colspan="1">Indice de Precipitacion Estandarizado - </td>
                           <td class="name " rowspan="1" colspan="1"> meses</td>
                           <td align="right" class="value " rowspan="1" colspan="1"> </td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
               </div>
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/%5B/%28.%29/%28SPI12%29//var/parameter/%5Dconcat/interp/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.5%2C-70%2C-30.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/name//long_name/exch/def/DATA/-2.5/2.5/RANGE/dup/T/fig-/colorbars2/-fig/+.gif" />  
            
         </fieldset> 
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/%5B/%28.%29/%28SPI12%29//var/parameter/%5Dconcat/interp/name//long_name/exch/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/X/-85./-65.0/plotrange/Y/-55.9/-17.7/plotrange//color_smoothing/null/psdef//antialias/true/psdef/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/%5B/%28.%29/%28SPI12%29//var/parameter/%5Dconcat/interp/name//long_name/exch/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/X/-85./-65.0/plotrange/Y/-55.9/-17.7/plotrange//color_smoothing/null/psdef//antialias/true/psdef/T/last/plotvalue+.gif"
                 border="0"
                 alt="image" /><br clear="none" />
           <img src="Escala_SPI_es.jpg" alt="IPE"> </img>
            
         </fieldset>
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Índice de Precipitación Estandardizado Nacional</h3>
            
            <p align="left" property="term:description">Este mapa muestra el índice de Precipitación Estandarizado (IPE) para múltiples periodos de acumulación.</p>
            
            <p align="left">El Índice de Precipitación Estandarizado (SPI; McKee 1993) es el número de desviaciones estándar que la precipitación acumulada
               se desvía del promedio climatológico. Esto indica que valores por debajo de un valor -1 indican condiciones de déficit significativos,
               mientras que valores mayores que +1 indican condiciones más húmedos que lo normal.
            </p>
            
            <p align="left">
               <h6> Tabla 1: Rango del Índice de Precipitación Estandarizada</h6>
            </p>
            
            <p align="left"><img src="SPI-scale_esp.jpg" alt="IPE"> </img></p>
            
            <p align="left">El IPE está disponible para diferentes periodos de acumulación: 1, 3, 6, 9 o 12-meses, lo cual permite evaluar la duración
               de las condiciones de sequía y superávit para diferentes escalas de tiempo. Selecciona la escala de tiempo de interés en el
               menú&gt;análisis. En el menú&gt;Región puedes seleccionar la región de interés. 
            </p>

            <p align="left"><b>Fuente de los datos</b> 
            </p>
            
            <p align="left"> El c&#225;lculo del &#237;ndice fue realizado con 55 estaciones provenientes de la Direcci&#243;n Meteorol&#243;gica de Chile (DMC) y de la Direcci&#243;n General de Aguas (DGA).
             </p>  
            <p align="left"> <img src="../../icons/Logo_dmc" alt="Logo DMC" /><img src="../../icons/Logo_dga" alt="Logo DGA" /></p> 
               
            
            
            <p align="left"><b>Referencias</b> 
            </p>
            
            <p align="left">McKee, T. B., N. J. Doesken, and J. Kliest, 1993:  The relationship of drought frequency and duration to time scales.  In
               <i>Proceedings of the 8th Conference of Applied Climatology, 17-22 January, Anaheim, CA</i>.  American Meteorological Society, Boston, MA. 179-184.
               
            </p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el IPE?</h3>
            
            <p align="left">El Índice de Precipitación Estandarizada (SPI; McKee 1993) es el número de desviaciones estándar que la precipitación cumulativa
               se desvía del promedio climatológico. Puede ser calculado por cualquier escala de tiempo. Aquí el IPE es calculado por 1,
               3, 6, 9 y 12-meses.
            </p>
            
            <p align="left">Se requiere una larga serie de tiempo de precipitación acumulada para el cálculo de este índice para cada escala de tiempo;
               con ello se puede estimar la función de densidad de probabilidad más apropiada, que para este caso es la distribución de Pearson-III
               (i.e., gamma 3-parámetros), según lo sugerido por Guttman (1999). Esta distribución es utilizada para ajustar la serie de
               tiempo de la precipitación. La distribución es posteriormente transformada a una distribución normal acumulada, el resultado
               es el IPE.
            </p>
            
            <p align="left">En la siguiente figura se presenta el procedimiento de transformación de las observaciones de precipitación a través de la
               función acumulada de probabilidad, distribución Gamma, a una variable normal estandarizado, el IPE.
            </p>
            
            <p align="left"><img src="EPI_calculation.png" alt="EPI"> </img></p>
            
            <p align="left">
               <h6>Figura 1: Transformación de la precipitación observada a través de la CDF de la distribución Gamma, al Índice de Precipitación
                  Estandarizada (IPE)
               </h6>
            </p>
            
            <p align="left">El IPE puede ser interpretado como una probabilidad usando una distribución normal estandarizada. Por ejemplo, la probabilidad
               de que el IPE tenga un valor entre -1 y +1 es del 68%.
            </p>
            
            <p align="left"><b>Referencias</b> 
            </p>
            
            <p align="left">Guttman, N. B., 1999:  Accepting the Standardized Precipitation Index:  A calculation algorithm. <i>J. Amer. Water Resour. Assoc.</i>., <b>35(2)</b>, 311-322.
               <br clear="none" />McKee, T. B., N. J. Doesken, and J. Kliest, 1993:  The relationship of drought frequency and duration to time scales.  In
               <i>Proceedings of the 8th Conference of Applied Climatology, 17-22 January, Anaheim, CA</i>.  American Meteorological Society, Boston, MA. 179-184.
               
            </p>
            
         </div>
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p align="left"><a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.SPI/"
                  shape="rect">Índice de Precipitación Estandarizada (IPE)</a>.
            <p align="left"> El c&#225;lculo del &#237;ndice fue realizado con 55 estaciones provenientes de la Direcci&#243;n Meteorol&#243;gica de Chile (DMC) y de la Direcci&#243;n General de Aguas (DGA).
             </p>  
            <p align="left"> <img src="../../icons/Logo_dmc" alt="Logo DMC" /><img src="../../icons/Logo_dga" alt="Logo DGA" /></p> 
            </p>
            
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel"> 
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile"
                  shape="rect">mwar_lac@unesco.org</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartit</legend>
         </fieldset>
         
      </div>
      
   </body>
 </html>
