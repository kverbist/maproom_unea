<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>&#205;ndice de Precipitaci&#243;n Estandardizado</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="SPI.html?Set-Language=en" />
<link class="share" rel="canonical" href="SPI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/a:/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/:a:/.Countrymask/.mask/mul//plotlast/3.5/def//plotfirst/-3.5/:a/def/DM_SPI_2p5_colors//name//SPI/def/T/last/VALUE%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-56/-16/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/+//XOVY+null+psdef//plotaxislength+550+psdef//plotborder+72+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="carry share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequ&#237;a Meteorol&#243;gica</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
            <fieldset class="navitem">
               <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var"><option value="">IPE 1 Mes</option><option value="SPI3">IPE 3 Meses</option><option value="SPI6">IPE 6 Meses</option><option value="SPI9">IPE 9 Meses</option><option value="SPI12">IPE 12 Meses</option><option value="SPI24">IPE 24 Meses</option><option value="SPI36">IPE 36 Meses</option><option value="SPI48">IPE 48 Meses</option></select>
            </fieldset>	
 	     <fieldset class="navitem"><legend>Promedio espacial sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
		<option value="0.5">Ubicación puntual</option>
		<option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
		<option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
		<option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Distrito</option>
		</select>
    		<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          	<select class="pageformcopy" name="region">
            	<optgroup class="template" label="Label">
            	<option class="iridl:values region@value label"></option>
              </optgroup>
            	</select>
            </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>

<fieldset class="dlimage regionwithinbbox">
<a class="dlimgts" rel="iridl:hasTable" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-65.75%2C-32.0%2C-66%2C-32.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI%29def//units/%28-%29def/T+exch+table-+text+text+skipanyNaN+-table/"></a>
<div style="float: left;">
<img class="dlimgloc" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/.Countrymask/.mask_monthly/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+//plotborder+0+psdef//plotaxislength+120+psdef+.gif" />
</div>	

<div style="float: left;">
	
<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgts" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
<div class="template"> Observacioness para <span class="bold iridl:long_name"></span>
</div>
</div>


<div class="valid" style="text-align: top;">
<div>Valores del IPE para el mes actual: </div> 
<a class="dlimgloc" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/.SPI/SPI1/T/last/VALUE/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI1:%20%29def/SPI3/T/last/VALUE/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI3:%20%29def/SPI6/T/last/VALUE/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI6:%20%29def/SPI9/T/last/VALUE/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI9:%20%29def/SPI12/T/last/VALUE/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI12:%20%29def/5/ds/info.json"></a>
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
<div><table class="valid template">
<tr style="color : black"><td class="name "></td><td align="right" class="value "></td></tr>

</table>
</div>

</div>
</div>

<br />
<img class="dlimgts regionwithinbbox" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/X/low/0.01/high/GRID/Y/low/0.01/high/GRID/%28bb:-65.75%2C-32.0%2C-66%2C-32.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SPI_discrete_colors/DATA/-2.5/2.5/RANGE//long_name/%28Indice%20de%20Precipitacion%20Estandardizado%29def/dup/T/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />  
</fieldset> 


<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/a:/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/:a:/.Countrymask/.mask/mul/:a//name//SPI/def/DATA/-2.5/2.5/RANGE//long_name/%28Indice%20de%20Precipitacion%20Estandardizado%29def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-56/-16/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/a:/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/:a:/.Countrymask/.mask/mul/:a//name//SPI/def/DATA/-2.5/2.5/RANGE//long_name/%28Indice%20de%20Precipitacion%20Estandardizado%29def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-56/-16/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/++//T/last/plotvalue/Y/-55.25/-17.25/plotrange+//plotborder+72+psdef//plotaxislength+550+psdef//XOVY+null+psdef+.gif"  border="0" alt="image" /><br />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/a:/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/:a:/.Countrymask/.mask/mul/:a//name//SPI/def/DATA/-2.5/2.5/RANGE//long_name/%28Indice%20de%20Precipitacion%20Estandardizado%29def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-56/-16/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/.auxfig+//T/last/plotvalue/Y/-55.25/-17.25/plotrange+//plotborder+72+psdef//plotaxislength+550+psdef//XOVY+null+psdef+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >&#205;ndice de Precipitaci&#243;n Estandarizado</h3>
<p align="left" property="term:description">Este mapa muestra el &#237;ndice de Precipitaci&#243;n Estandarizado (IPE) para m&#250;ltiples periodos de acumulaci&#243;n.</p>
<p align="left">El &#205;ndice de Precipitaci&#243;n Estandarizado (SPI; McKee 1993) es el n&#250;mero de desviaciones est&#225;ndar que la precipitaci&#243;n acumulada se desv&#237;a del promedio climatol&#243;gico. Esto indica que valores por debajo de un valor -1 indican condiciones de d&#233;ficit significativos, mientras que valores mayores que +1 indican condiciones m&#225;s h&#250;medos que lo normal.</p>
<p align="left"><h6> Tabla 1: Rango del &#205;ndice de Precipitaci&#243;n Estandarizada</h6></p>
<p align="left"><img src="Escala_SPI_esp.jpg" alt="IPE"> </img></p>
<p align="left">El IPE est&#225; disponible para diferentes periodos de acumulaci&#243;n: 1, 3, 6, 9, 12, 24, 36 o 48-meses, lo cual permite evaluar la duraci&#243;n de las condiciones de sequ&#237;a y super&#225;vit para diferentes escalas de tiempo. Selecciona la escala de tiempo de inter&#233;s en el men&#250;>an&#225;lisis. En el men&#250;>Regi&#243;n puedes seleccionar la regi&#243;n de inter&#233;s. </p>
<p align="left"><b>Referencias</b> </p>
<p align="left">McKee, T. B., N. J. Doesken, and J. Kliest, 1993:  The relationship of drought frequency and duration to time scales.  In <i>Proceedings of the 8th Conference of Applied Climatology, 17-22 January, Anaheim, CA</i>.  American Meteorological Society, Boston, MA. 179-184.
</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula el IPE?</h3>
<p align="left">El &#205;ndice de Precipitaci&#243;n Estandarizada (SPI; McKee 1993) es el n&#250;mero de desviaciones est&#225;ndar que la precipitaci&#243;n cumulativa se desv&#237;a del promedio climatol&#243;gico. Puede ser calculado por cualquier escala de tiempo. Aqu&#237; el IPE es calculado por 1, 3, 6, 9 y 12-meses.</p>
<p align="left">Se requiere una larga serie de tiempo de precipitaci&#243;n acumulada para el c&#225;lculo de este &#237;ndice para cada escala de tiempo; con ello se puede estimar la funci&#243;n de densidad de probabilidad m&#225;s apropiada, que para este caso es la distribuci&#243;n de Pearson-III (i.e., gamma 3-par&#225;metros), seg&#250;n lo sugerido por Guttman (1999). Esta distribuci&#243;n es utilizada para ajustar la serie de tiempo de la precipitaci&#243;n. La distribuci&#243;n es posteriormente transformada a una distribuci&#243;n normal acumulada, el resultado es el IPE.</p>
<p align="left">En la siguiente figura se presenta el procedimiento de transformaci&#243;n de las observaciones de precipitaci&#243;n a trav&#233;s de la funci&#243;n acumulada de probabilidad, distribuci&#243;n Gamma, a una variable normal estandarizado, el IPE.</p>
<p align="left"><img src="EPI_calculation.png" alt="EPI"> </img></p>
<p align="left"><h6>Figura 1: Transformaci&#243;n de la precipitaci&#243;n observada a trav&#233;s de la CDF de la distribuci&#243;n Gamma, al &#205;ndice de Precipitaci&#243;n Estandarizada (IPE)</h6></p>
<p align="left">El IPE puede ser interpretado como una probabilidad usando una distribuci&#243;n normal estandarizada. Por ejemplo, la probabilidad de que el IPE tenga un valor entre -1 y +1 es del 68&#37;.</p>
<p align="left"><b>Referencias</b> </p>
<p align="left">Guttman, N. B., 1999:  Accepting the Standardized Precipitation Index:  A calculation algorithm. <i>J. Amer. Water Resour. Assoc.</i>., <b>35(2)</b>, 311-322.
<br />McKee, T. B., N. J. Doesken, and J. Kliest, 1993:  The relationship of drought frequency and duration to time scales.  In <i>Proceedings of the 8th Conference of Applied Climatology, 17-22 January, Anaheim, CA</i>.  American Meteorological Society, Boston, MA. 179-184.
</p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p align="left"><a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/.SPI/">&#205;ndice de Precipitaci&#243;n Estandarizada (IPE)</a>, entregado por <a href="http://www.esrl.noaa.gov/psd/data/gridded/data.unified.html">NOAA NCEP Climate Prediction Center</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartit</legend></fieldset>
</div>
 </body>
 </html>
