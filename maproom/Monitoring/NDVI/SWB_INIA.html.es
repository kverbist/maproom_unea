<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Balance H&#237;drico del Suelo (INIA)</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="SWB_INIA.html?Set-Language=en" />
<link class="share" rel="canonical" href="SWB_INIA.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"/>
<link rel="term:icon" href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.INIA/.SoilMoisture/.Soil_Moisture_deficit/X+Y+fig-+colors+coasts+-fig+//T/3777.0/plotvalue/Y/-44.99605/-28.99311/plotrange+//plotaxislength+432+psdef//plotborder+72+psdef//XOVY+null+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg dlimgts share" name="anal" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgloc dlimgts" name="region" type="hidden" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />   
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Sequ&#237;a Agr&#237;cola</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
         <fieldset class="navitem">
            		
            <legend>An&#225;lisis</legend>
            	<span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value="">Porcentaje</option>
               <option value="Anomaly">Anomalia</option>
               <option value="Observed">Observado</option></select></fieldset>

         <fieldset class="navitem">
            <legend>Promedio Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option></select>
            	    
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select> 
            
         </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>
 <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//units/%28%25%29def//long_name/%28Humedad%20del%20Suelo%20Observada%29def/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%29def//units/%28%25%29def/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Anomal%C3%ADa%20Estandarizada%29def/3/array/astore/%7BT/last/dup/12.0/sub/exch/RANGE/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject%5BX/Y%5Dweighted-average%7Dforall/T//long_name/%28Tiempo%29def/4/-3/roll/table:/4/:table/"
               shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.SoilMoisture/.Soil_Moisture/T/%2830%20Mar%202015%20-%206%20Apr%202015%29/VALUE/0/flagge/X/Y/%28bb:-75.00416%2C-44.99605%2C-69.00175%2C-28.99311%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.01%2C-36.01%2C-71.0%2C-36.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/T/last/plotvalue+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               	
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  <div class="template">Observaciones para <span class="bold iridl:long_name"></span>
                     
                  </div>
                  
               </div>
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  <div> Valores de Humedad del Suelo para el mes actual: </div> 
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Standardized%20Anomaly%29def/T/last/VALUE/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28Anomal%C3%ADa%20Estandarizada%20de%20la%20Humedad%20del%20Suelo%20%28-%29:%20%29def/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage/%28percent%29unitconvert//long_name/%28Soil%20Moisture%20-%20Percentage%20of%20Normal%29def/T/last/VALUE/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%20%28%25%29:%20%29def/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//long_name/%28Observed%20Soil%20Moisture%29def/T/last/VALUE/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28Humedad%20del%20Suelo%20Observada%20%28%25%29:%20%29def/3/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
                  
               </div>
               
            </div>
             
            <br clear="none" />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Anomalia%20Estandarizada%20de%20la%20Humedad%20del%20Suelo%29/def/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//long_name/%28Humedad%20del%20Suelo%20Observada%29/def/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%29/def/%28bb:-72.0%2C-37.5%2C-71.95%2C-37.25%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/T/fig-/colorbars2/-fig%7Dif/+.gif" />  
            
         </fieldset>
          
         <fieldset class="dlimage" id="content" about="">
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Anomal%C3%ADa%20Estandarizada%20de%20la%20Humedad%20del%20Suelo%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//long_name/%28Humedad%20del%20Suelo%20Observada%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage/DATA/0/100/RANGE//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Anomal%C3%ADa%20Estandarizada%20de%20la%20Humedad%20del%20Suelo%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//long_name/%28Humedad%20del%20Suelo%20Observada%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage/DATA/0/100/RANGE//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif/+.gif"
                 border="0"
                 alt="image" /><br clear="none" />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_st_anomaly//long_name/%28Anomal%C3%ADa%20Estandarizada%20de%20la%20Humedad%20del%20Suelo%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture//long_name/%28Humedad%20del%20Suelo%20Observada%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//Soil_Moisture_deficit/0.0/100.0/plotrange//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7B/SOURCES/.Chile/.INIA/.SoilMoisture/.Monthly/.Monthly_soil_moisture_percentage/DATA/0/100/RANGE//long_name/%28Humedad%20del%20Suelo%20-%20Porcentaje%20del%20Promedio%29/def/X/Y/fig-/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/-fig//T/last/plotvalue//Y/-44.99605/-28.99311/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif/+.auxfig/+.gif" />
            
         </fieldset>

 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >Balance H&#237;drico del Suelo (INIA) </h3>
<p align="left" property="term:description">Este mapa muestra el Balance H&#237;drico del Suelo para Chile-Central, calculado por el Instituto de Investigaciones Agropecuarias (INIA) Quilamapu.</p>
<p align="left">El Balance H&#237;drico del Suelo refleja el contenido de humedad en el suelo con respecto a su valor m&#225;ximo (la 'Capacidad de Campo') y tiene unidad de porcentaje.</p>
<p align="left">Un valor por debajo de 50 por ciento indica una reducci&#243;n en la disponibilidad de agua en el suelo cual puede causar estr&#233;s en la vegetaci&#243;n.</p>
<p align="left">Los datos en este mapa provienen del Instituto de Investigaciones Agropecuarias (INIA).</p>
<p align="left">  <img src="../../icons/Logo_inia" alt="Logo INIA"></img></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula el Balance H&#237;drico del Suelo? </h3>
<p>La disponibilidad de agua en el suelo (almacenamiento), por Regi&#243;n, se estima a partir de datos proporcionados por im&#225;genes satelitales.</p>
<p>Para el c&#225;lculo de disponibilidad de agua en el suelo es necesario estimar las variables de evapotranspiraci&#243;n, precipitaci&#243;n,  almacenamiento de agua en el suelo en el periodo anterior, esto queda representado por la Ecuaci&#243;n 1. Las capas necesarias para la determinaci&#243;n de estas variables son: Uso de suelo, propiedades, f&#237;sico h&#237;dricas del suelo (capacidad de campo, punto de marchitez permanente, profundidad de suelo, etc.),  Modelos digitales de elevaci&#243;n, subdivisi&#243;n pol&#237;tico administrativa de Chile, objetos naturales o artificiales existentes en el territorio (lagos, ciudades, etc.)</p>
<p align="left"><img src="SWB_formula1.png" alt="NDVI"> </img></p>
<h6>Ecuaci&#243;n 1: c&#225;lculo de disponibilidad de agua en el suelo</h6>
<p>Donde <img src="SWB_formula2.png" alt="NDVI"> </img> corresponden al almacenamiento de agua en el suelo para el periodo actual y para el periodo anterior, respectivamente; Ppe es la precipitaci&#243;n efectiva, Riego es el riego efectuado en el lugar (Riego = 0), ET es la evapotranspiraci&#243;n, Esc es la escorrent&#237;a y  Pprof corresponde a la percolaci&#243;n profunda.</p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p align="left"><a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.INIA/.SoilMoisture/.Soil_Moisture_deficit/">Balance H&#237;drico del Suelo</a>, entregado po Instituto Nacional de Investigacion Agropecuaria <a href="http://www.inia.cl/">(INIA)</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>

<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
