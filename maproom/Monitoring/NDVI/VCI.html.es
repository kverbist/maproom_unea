<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Índice de la Condición de la Vegetación (VCI)</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="VCI.html?Set-Language=en" />
<link class="share" rel="canonical" href="VCI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/%28FullArea%29//ana/parameter/%28FullArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCIv2/.VCI_250m/.VCI/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Vegetation%20Condition%20Index%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif//ana/get_parameter/%28AgriculturalArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCIv2/.VCI_250m/.VCI/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Vegetation%20Condition%20Index%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif+//plotborder+0+psdef//plotaxislength+646+psdef//color_smoothing+1+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
<script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script>
<style xml:space="preserve">
.dlimgtsbox { 
width: 49%;
display: inline-block
 }
body.varreflectance img.dlauximg{display: none}
body.varreflectance .regionwithinbbox{display: none !important}
</style></head>
<body xml:lang="es">

      <form name="pageform" id="pageform"
            class="info carryup carry share dlimg dlauximg dlimgts dlimgloc"
            method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
         <input class="dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg dlimgts onlyvar share" name="ana" type="hidden" data-default="FullArea"/>
         <input class="share dlimgts dlimgloc" name="region" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="unused dlimg" name="plotaxislength" type="hidden" value="432" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" /> 
         
         
         
      </form>
      
      
      <div class="controlBar">
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            <legend>Chile</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Sequía Agrícola</span></legend>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem">
            
            
            <legend>Regi&#243;n</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Region">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
            
         </fieldset>
         
         
         <fieldset class="navitem">
            
            
            <legend>An&#225;lisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="ana">
               <option value="FullArea">&#193;rea completo</option>
               <option value="AgriculturalArea">&#193;rea Agr&#237;cola</option></select>
            
            
         </fieldset>
         	     
         
         <fieldset class="navitem">
            
            
            <legend>Promedio Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option></select>
            	    
            
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select> 
            
            
            
         </fieldset>
         
         
      </div>
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km//long_name/%28VCI%29def/T/last/dup/365/sub/exch/RANGE/0/masklt/%28bb:-73.0%2C-38.5%2C-72.5%2C-38.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km_maskAgro//long_name/%28VCI_ZonaAgricola%29def/T/last/dup/365/sub/exch/RANGE/0/masklt/%28bb:-73.0%2C-38.5%2C-72.5%2C-38.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T/3/-2/roll/table-/text/text/text/skipanyNaN/-table/"
               shape="rect"></a>
            
            
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DMC/.SPI/.SPI1/T/%28Dec%202014%29/VALUE/-50/flaggt/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-70.5%2C-30.0%2C-71.0%2C-30.5%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef/T/last/plotvalue+.gif" />
               
               
               
               
            </div>	
            
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  
                  
                  
                  <div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  
                  <div> Valores del VCI para el mes actual:</div> 
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km//long_name/(VCI)/def/T/last/VALUE/0/masklt/(bb%3A-73.0%2C-38.5%2C-72.5%2C-38.0)//region/parameter/geoobject/[X/Y]weighted-average/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km_maskAgro//long_name/(VCI%20para%20la%20Zona%20Agricola)/def/T/last/VALUE/0/masklt/(bb%3A-73.0%2C-38.5%2C-72.5%2C-38.0)//region/parameter/geoobject/[X/Y]weighted-average/2/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
               </div>
               
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km/T/last/dup/540/sub/exch/RANGE/X/-75.0/0.1/-67.0/GRID/Y/-18.0/0.1/-55.5/GRID/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/0/masklt/%28bb:-73.0%2C-38.5%2C-72.5%2C-38.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/DATA/0/100/RANGE/dup/T/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef/+.gif" />  
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km/T/last/dup/540/sub/exch/RANGE/X/-75.0/0.1/-67.0/GRID/Y/-18.0/0.1/-55.5/GRID/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/0/masklt/%28bb:-73.0%2C-38.5%2C-72.5%2C-38.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/DATA/0/100/RANGE/dup/T/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef/+.auxfig/+.gif" />
            
            
            
         </fieldset> 
         
         
         
         
         <fieldset class="dlimage">
            <a class="justsregion" rel="iridl:hasFigure"
               href="http://www.climatedatalibrary.cl/%28FullArea%29//ana/parameter/%28FullArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif//ana/get_parameter/%28AgriculturalArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km_maskAgro//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif/"
               shape="rect"></a>
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/%28FullArea%29//ana/parameter/%28FullArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif//ana/get_parameter/%28AgriculturalArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km_maskAgro//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif/+.gif"
                 border="0" />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/%28FullArea%29//ana/parameter/%28FullArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif//ana/get_parameter/%28AgriculturalArea%29/eq/%7B/SOURCES/.Chile/.INIA/.VCI_Analysis2/.VCI_1km/.VCI_1km_maskAgro//long_name/%28%C3%8Dndice%20de%20la%20Condici%C3%B3n%20de%20la%20Vegetaci%C3%B3n%29/def/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/Y/-56.0/-17.505/plotrange/X/-78/-66/plotrange/T/last/plotvalue%7Dif/+.auxfig/+.gif" />
            
            
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h3 align="center" property="term:title">Índice de la Condición de la Vegetación (VCI)</h3>
            
            
            <p align="left" property="term:description">Este mapa muestra la condición de la vegetación actual, como reflejado por el Índice de la Condición de la Vegetación (VCI).
               
            </p>
            
            
            <p>El VCI compara el valor del NDVI actual con el rango de valores del NDVI de anteriores a&#241;os para la misma ubicaci&#243;n.
		 El VCI se expresa en porcentaje y muestra la ubicaci&#243;n del valor observado entre el m&#225;ximo y m&#237;nimo observado en a&#241;os anteriores.       
               Valores bajas indican condiciones de sequ&#237;a agr&#237;cola y valores altas condiciones &#243;ptimas de la vegetaci&#243;n.
            </p>
            
            
            <p>Se puede visualizar el VCI para todo el pais o para la zona agr&#237;cola solamente. Selecciona la variable de interés en el menú&gt;análisis. En el menú&gt;región
               se puede seleccionar la región de interés.
               
            </p>
            
            
                   
            
            <p align="left">
               
               <h6> Tabla 1: Interpretación del Índice de la Condición de la Vegetación (VCI)</h6>
               
            </p>
            
            
            <p align="left"><img src="Escala_VCI_esp.jpg"
                    alt="Leyenda del VCI"> </img></p>
            
                      
            <p align="left">Los mapas del VCI para Chile son preparados por el Instituto de Investigaciones Agropecuarias (INIA).</p>
            
            
            <p align="left">  <img src="../../icons/Logo_inia" alt="Logo INIA" /></p>
            
            
            
         </div>
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            <h3 align="center">¿Cómo se calcula el VCI?</h3>
            
            <p>
               El VCI es calculado de la siguiente forma:
               
               
            </p>
            
            
            <p align="left"><img src="VCI_formula.gif" alt="VCI"> </img></p>
            
            
            <p>Donde NDVI es el m&#237;nimo y m&#225;ximo del &#205;ndice de la Diferencia Normalizada de la Vegetaci&#243;n (NDVI) observada durante el mismo periodo de 16 d&#237;as.
               
            </p>

            
            <p>
               El NDVI es calculado de la siguiente forma:
               
               
            </p>
            
            
            <p align="left"><img src="NDVI_formula.png" alt="NDVI"> </img></p>
            
            
            <p>Donde el VIS es el reflejo espectral en la región visible (rojo) expresado como ratio (reflejo/entrante) y el NIR es el reflejo
               espectral en la región infrarrojo cercano expresado como ratio (reflejo/entrante).
               
            </p>
            
            
            
            <p>En la figura abajo vez una ilustración de una calculación de NDVI.</p>
            
            
            <p align="left"><img src="NDVI_figura.png" alt="NDVI"> </img></p>
            
            
            <p align="left">
               
               <h6>Figura 1: Ejemplo de calculación del NDVI</h6>
               
            </p>
            
            
            <h3 align="center">¿Qué es la interacción entre la vegetación y el NIR y VIS?</h3>
            
            
            <p>Plantas absorben y reflejan regiones particulares del espectro. Gran parte del VIS es absorbido por plantas como energía para
               la fotosíntesis. Gran parte del NIR es reflejado por plantas por qué no puede ser utilizado en la fotosíntesis. Cuando plantas
               están en malas condiciones hay un cambio en el reflejo y absorbencia del VIS y NIR.
               
            </p>
            
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            
            <h3 align="center">Fuente de los Datos</h3>
            
            
            <p align="left"> <a href="http://climatedatalibrary.cl/SOURCES/.Chile/.INIA/.VCI"
                  shape="rect">VCI</a>, entregado por United States Geological Survey, Land Processes Distributed Active Archive Center, 
               Moderate Resolution Imaging Spectroradiometer <a href="http://iridl.ldeo.columbia.edu/SOURCES/.USGS/.LandDAAC/.MODIS/.version_005/.dataset_documentation.html"
                  shape="rect">USGS LandDAAC MODIS</a></p>
            
            
         </div>
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            <h3 align="center">Soporte</h3>
            
            
            <p>
               Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=VCI Chile" shape="rect">mwar_lac@unesco.org</a>
               
               
            </p>
            
            
         </div>
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            
            <h3 align="center">Instrucciones</h3>
            
            
            <p>
               El interface con datos de MODIS permite a usuarios de crear gráficos. El interface entrega los más recientes imagenes de MODIS,
               entregando mapas interactivos de VCI. 
               
               
            </p>
            
            
            <p> El usuario puede seleccionar un punto de interés por lo cual se genera una serie de tiempo del VCI para los
               últimos 18 meses. 
               
               
            </p> 
            
            
            
            
            <div class="buttonInstructions"></div>
            
            
         </div>
         
         
      </div>
      
      
      <div class="optionsBar">
         
         
         <fieldset class="navitem" id="share">
            
            <legend>Compartir</legend>
            
         </fieldset>
         
         
      </div>
      
      
   </body>
</html>