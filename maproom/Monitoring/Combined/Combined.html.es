<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Indice de Sequ&#237;a Combinado</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Combined.html?Set-Language=en" />
<link class="share" rel="canonical" href="Combined.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Combined_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016//long_name/%28Indice%20de%20Sequia%20Combinado%29/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/Y/-56.95/-16.95/plotrange/X/-78/-66/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/T/last/plotvalue+//plotborder+0+psdef//color_smoothing+1+psdef//plotaxislength+671+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share"
            method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="dlimg dlauximg share" name="anal" type="hidden" />
         <input class="share dlimgloc dlimgts" name="region" type="hidden" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Combined_term"><span property="term:label">Indice Sequia Combinado</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         	    
         <fieldset class="navitem">
            <legend>Promedio espacial sobre</legend><span class="selectvalue"></span>
           <select class="pageformcopy" name="resolution">
               <option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
           </select>
            	    
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/%28bb:-70.5%2C-30.5%2C-70%2C-30.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28CDI%29def//units/%28-%29def/T/last/dup/18/sub/exch/RANGE/T/exch/table-/text/text/skipanyNaN/-table/"
               shape="rect"></a>
            
            
          
            
            
            <div style="float: left;">
               	
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  
                  <div class="template">  Observacioness para <span class="bold iridl:long_name"></span>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  <div>
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/T/last/VALUE/(bb%3A-72.50%2C-36.5%2C-72.0%2C-36.0)//region/parameter/geoobject/[X/Y]weighted-average//long_name/(Combined%20Drought%20Index%3A%20)/def/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:values": {
 "td.time+": "var.T"
}
}
}
</script>
                  <div>           
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="time " rowspan="1" colspan="1">El valor m&#225;s reciente est&#225; disponible para el mes de </td>
                        </tr>
                     </table>
                  </div>                  
                  </div>

               <div class="valid" style="display: inline-block; text-align: left; ">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.Alert_SPI/T/last/VALUE/1//alertID/parameter/dup/flagrange/(irids%3ASOURCES%3AFeatures%3APolitical%3AChile%3Acomunas%3Agid%40170%3Ads)//region/parameter/geoobject/[X/Y]weighted-average/100/mul//long_name/(Superficie%20con%20Alerta%201)/def//units/(%25)/def/toi4/info.json"
                     shape="rect"></a>
                  <div class="template">  &#193;rea actual bajo sequ&#237;a meteorol&#243;gica (Alerta 1): <span class="bold iridl:value"></span><b>%</b>
                  </div>
               </div>
                
               <div class="valid" style="display: inline-block; text-align: left;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.Alert_ICE/T/last/VALUE/1//alertID/parameter/dup/flagrange/(irids%3ASOURCES%3AFeatures%3APolitical%3AChile%3Acomunas%3Agid%40170%3Ads)//region/parameter/geoobject/[X/Y]weighted-average/100/mul//long_name/(Superficie%20con%20Alerta%202)/def//units/(%25)/def/toi4/info.json"
                     shape="rect"></a>
                  <div class="template">  &#193;rea actual bajo sequ&#237;a hidrol&#243;gica (Alerta 2): <span class="bold iridl:value"></span><b>%</b>                     
                  </div>
               </div> 
           
               <div class="valid" style="display: inline-block; text-align: left;">

                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/T/last/VALUE/3//alertID/parameter/dup/flagrange/(irids%3ASOURCES%3AFeatures%3APolitical%3AChile%3Acomunas%3Agid%40170%3Ads)//region/parameter/geoobject/[X/Y]weighted-average/100/mul//long_name/(Superficie%20con%20Alerta%203)/def//units/(%25)/def/toi4/info.json"
                     shape="rect"></a>
                  <div class="template">  &#193;rea actual bajo sequ&#237;a agr&#237;cola (Alerta 3): <span class="bold iridl:value"></span><b>%</b>
                </div>          
               </div>

<div>  
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/T/last/VALUE/(bb%3A-72.50%2C-36.5%2C-72.0%2C-36.0)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(Promedio%20Comunal%20del%20%C3%8Dndice%20de%20Sequ%C3%ADa%20Combinado%3A%20)/def/1/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
</div>

         </div>    
        </div>
 
   

                  
            
            <br clear="none" />
             <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/T/last/dup/36/sub/exch/RANGE/%28bb:-72.50%2C-36.5%2C-72.0%2C-36.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//scale_min/0/def//scale_max/3/def//long_name/%28%29/def//CLIST%5B/%28Sin%20Sequ%C3%ADa%29/%28Sequ%C3%ADa%20Meteorol%C3%B3gica%29/%28Sequ%C3%ADa%20Hidrol%C3%B3gica%29/%28Sequ%C3%ADa%20Agr%C3%ADcola%29/%5Ddef/startcolormap/transparent/white/white/yellow/orange/brown/brown/endcolormap/dup/T/fig-/colorbars2/-fig//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" />            
            <img class="dlauximg" src="Niveles_CDI_2016_esp.jpg" />  
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/T/last/dup/36/sub/exch/RANGE/3//alertID/parameter/dup/flagrange/%28irids:SOURCES:Features:Political:Chile:comunas:gid%40170:ds%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//region/get_parameter/geoobject/geometryarea/mul/%28irids:SOURCES:Features:Political:Chile:comunas:gid%40170:ds%29//region/parameter/geoobject/geometryarea/div/100/mul//long_name/%28Superficie%20bajo%20Sequ%C3%ADa%20Agr%C3%ADcola%29/def//units/%28%25%29/def/windspeed_colors/DATA/0/AUTO/RANGE/dup/T/fig-/colorbars2/-fig/+.gif" />         
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.SPI6min/.SPI6min/T/last/dup/36/sub/exch/RANGE/%28bb:-71.10%2C-30.1%2C-71.0%2C-30.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/DATA/AUTO/AUTO/RANGE/dup/dup/0/mul/-0.84/add//fullname/%28Umbral%20para%20SPI%29/def/T/fig-/colorbars2/red/thick/dotted/line/-fig//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /> 
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.ICE6min/.ICE6min/T/last/dup/36/sub/exch/RANGE/%28bb:-71.10%2C-30.1%2C-71.0%2C-30.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28Indice%20de%20Caudal%20Estandarizado%29/def/DATA/AUTO/AUTO/RANGE/dup/dup/0/mul/-0.84/add//fullname/%28Umbral%20para%20ICE%29/def/T/fig-/colorbars2/red/thick/dotted/line/-fig//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /> 
            <img class="dlimgts regionwithinbbox"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.NDVI_subset/.NDVI_subset/T/last/dup/36/sub/exch/RANGE/%28bb:-71.10%2C-30.1%2C-71.0%2C-30.0%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28Anomal%C3%ADa%20de%20la%20NDVI%29/def/DATA/AUTO/AUTO/RANGE/dup/dup/0/mul/-0.5/add//fullname/%28Umbral%20para%20NDVI%29/def/T/fig-/colorbars2/red/thick/dotted/line/-fig//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /> 
            
     
            
         </fieldset> 
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016//long_name/%28Indice%20de%20Sequia%20Combinado%29/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/Y/-56.95/-16.95/plotrange/X/-78/-66/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/T/last/plotvalue/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016//long_name/%28Indice%20de%20Sequia%20Combinado%29/def/a-/-a/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig/Y/-56.95/-16.95/plotrange/X/-78/-66/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/T/last/plotvalue+.gif"
                 border="0"
                 alt="image" /> <br clear="none" />
            <img class="dlauximg" src="Niveles_CDI_2016_esp.jpg" />
            
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Índice de Sequía Combinado</h3>
            
            <p align="left" property="term:description">Este mapa muestra el Índice de Sequia Combinado (CDI), el cual informa sobre la condición actual de sequía en Chile.</p>
            
            <p align="left">El Índice de Sequía Combinado integra indicadores de la sequía meteorológica (Índice de Precipitación Estandarizado, IPE),
               la sequía hidrológica (la anomal&#237;a de los caudales o ICE) y la sequía agrícola (la anomal&#237;a de la vegetaci&#243;n o NDVI).
            </p> 
            
            <p align="left">El CDI considera tres niveles de impacto:</p>
                      
            <p align="left"><img src="Escala_CDI_2016_esp.jpg" alt="Niveles_CDI"> </img></p>
            
         </div>
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se determina los tres niveles de impacto?</h3>
            
            <p>
               Dependiendo los valores del IPE y anomalías de los caudales y del NDVI las observaciones del CDI son clasificados en los tres niveles
               de impacto. La siguiente tabla define los criterios de clasificación:
            </p>
            
            <p align="left"><img src="Escala_CDI_2016_esp.jpg" alt="Niveles_CDI"> </img></p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p align="left"><a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.CDI/.CDI_2016/.CombinedDroughtIndex_2016/"
                  shape="rect">Índice de Sequía Combinado</a>, calculado con el Índice de Precipitación Estandardizado <a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.DMC/.SPI/"
                  shape="rect">(IPE)</a>, el Índice de la anomalía de los caudales <a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.CAZALAC/.ICE/"
                  shape="rect">(ICE)</a> y el Índice de la Diferencia de la vegetación Normalizada <a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.INIA/.NDVI_Analysis/.NDVI_1km/.NDVI_1km_st_anomaly/"
                  shape="rect">(NDVI)</a></p>
            
            <p align="left">
               <h6>Tabla 1: Niveles de impacto del CDI</h6>
            </p>
            
            <p align="left"><img src="Escala_CDI_2016_esp.jpg" alt="Niveles CDI"> </img></p>
            
         </div>
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:mwar-lac@unesco.org?subject=Indice de sequia combinado Chile"
                  shape="rect">mwar-lac@unesco.org</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>

 </html>
